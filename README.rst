SPHN Documentation
==================

Documentation about the SPHN Semantic Interoperability Framework: https://sphn-semantic-framework.readthedocs.io


Installation
------------

To load the repository locally, do on a command line interface:
``git clone git@gitlab.com:sabine_oesterle/template.git``

**Software dependency**: To build the documentation locally, Python and Sphinx (https://www.sphinx-doc.org) are required. You many need to install the following Python packages (e.g., via ``pip``):

* ``pip install sphinx``
* ``pip install sphinx-rtd-theme``

Edits can be made to the ``.rst`` files to update the content of the documentation.

Local edits can be visualized by doing the following: 

* ``make html`` in the ``docs`` folder
* Open ``_build/html/index.html`` in your local browser to visualize the changes.

For a style guide see [style-guide](style-guide.md) 

When ready, changes must be pushed to the gitlab repository:

* ``git pull``
* ``git add <file>``
* ``git commit -m "<commit message>"``
* ``git push``

Support
----------

If you are having issues, please contact: dcc@sib.swiss

License
----------

The SPHN Dataset and SPHN RDF Schema are licensed under the CC-BY V4 License, License text https://creativecommons.org/licenses/by/4.0/.
