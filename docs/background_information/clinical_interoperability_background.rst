RDF in SPHN
============

.. note:: 
 > Watch our Webinar to learn more about linked data in SPHN https://www.youtube.com/watch?v=ZfdeJkKkhoE.

History
------------

The “Data Exchange Format” Task Force of the SPHN HospitalIT Working group evaluated several solutions for SPHN, such as common data models (e.g., `i2b2 <https://www.i2b2.org/>`_, and `OMOP <https://www.ohdsi.org/data-standardization/the-common-data-model/>`_), data exchange standards/formats (e.g., `FHIR <https://www.hl7.org/fhir/>`_), semantic-based descriptive framework (e.g., `RDF <https://www.w3.org/RDF/>`_) and flat files. 

The following evaluation criteria were used: 

* Coverage of the SPHN dataset
* Extensibility
* Scalability
* Understandability
* Adoption cost for researchers
* Adoption cost for the hospitals
* Supported value sets; Worldwide adoption
* Available tools and Stability

During the evaluation process it became clear that the use of existing common data models would introduce several limitations. Data models (sets of tables and relationships between them) are fixed representations of knowledge with each model created for a specific purpose. There is no “one-size-fits-all” data model that could serve all SPHN use cases. If multiple data models were used within SPHN the cost of mapping local data sources to each separate model would be unsustainable for hospitals and other data providers. If on the other hand a single data model was adopted it would likely not (totally) fulfill the needs of projects and would lead to either a loss of information or force individual projects to devise temporary ad hoc solutions. Data models are however hard to extend or modify without losing compatibility with existing mapping tools or analytics built on top of them.

As a result the Task Force recommended the adoption of a more flexible and extensible descriptive language solution for exchanging data and meta-data within SPHN. Such a descriptive language must be able to formally represent all current and any future SPHN concepts without information loss or the need for convoluted mappings. The Task Force specifically recommended the use of the Resource Description Framework (RDF) -  a universal solution to model information that is described in a series of semantic concepts such as the `SPHN Dataset <../sphn_framework/sphndataset.html>`_ (Pillar 1). With RDF, SPHN concepts and their instances (i.e., the data) can easily be mapped from/to other data representations or merged with other RDF data sets without losing their semantics. 

In November 2019 a two-day hackathon resulted in the successful integration of mock data from all five university hospitals in RDF. In 2020 several Driver projects ran pilot projects to further test RDF as an exchange format. In autumn of 2020 the `SPHN National Steering Board <https://sphn.ch/organization/governance/national-steering-board/>`_ (NSB) endorsed the use of RDF as one of the formats of choice for data transport and storage in all SPHN projects, and recommended the adoption of RDF as one of the SPHN approved data exchange standards.

Learn about how to use linked data in SPHN
------------------------------------------
https://sphn.ch/training/sphn-rdf-training-primer/
