.. _background-semantic-web:

Semantic Web in SPHN 
=====================

The term “Semantic Web” was coined by Tim Berners-Lee in 1999 and describes his vision for a linked, interconnected web. "The Semantic Web provides a common framework that allows data to be shared and reused across application, enterprise, and community boundaries" `[1] <https://www.w3.org/2001/sw/>`_. The Semantic Web is also referred to as ‘Linked Data’ or ‘Web of Data,’ and aspires to make the Web a global database with machine-interpretable metadata that connects related information together. In order for the Web to be linked, the Semantic Web requires data to be standardized and relationships among data to be made explicit.
 
Berners-Lee illustrated the architecture of the Semantic Web as a tower (Figure 1).

.. image:: ../images/background/semantic_web.png 
 :align: center 
 :alt: Semantic web tower

**Figure 1. Semantic Web Tower** (*source: https://en.wikipedia.org/wiki/Semantic_Web*)

Each layer in the tower builds on the one below, and all layers are integral parts to the Semantic Web. In the context of the SPHN, it is particularly worth highlighting the following layers of the Semantic Web: URI, RDF, RDFS, OWL, SPARQL and SKOS 

**URI** – stands for Uniform Resource Identifier. As the name suggests, with a URI, a resource can be identified with a unique identifier. One can think of a URI as the address of a resource.    

**RDF** – stands for Resource Description Framework and it is the core data model of the Semantic Web. Background information on RDF can be found :ref:`here <background-rdf>`. 

**RDFS** – stands for RDF Schema and it is the semantic extension of RDF. The SPHN schema uses the following RDFS components: ``rdfs:Class``, ``rdfs:label`` and ``rdfs:comment``. The ``rdfs:Class`` declares a resource as a class, a ``rdfs:label`` describes the class name and the ``rdfs:comment`` provides a description of the class in human-readable format. 

**OWL** – stands for Web Ontology Language. OWL documents are known as ontologies, and ontologies allow to organize data into concepts and relationships. In the SPHN, classes, object properties and data properties are translated into ``owl:Class``, ``owl:ObjectProperty`` and ``owl:DatatypeProperty``. 

**SPARQL** –  stands for SPARQL Protocol and RDF Query Language, and it is the query language of the Semantic Web, used for exploring RDF data. Background information on SPARQL can be found :ref:`here <background-sparql>`.  
 
**SKOS** – stands for Simple Knowledge Organization System, and it is used to design knowledge organization systems to enrich data with meaning. For example, a ``skos:definition`` encoding in the SPHN RDF schema can specify the values that are allowed for a specific concept (see the section on :ref:`enriching-knowledge-content`). 

References
-----------

* `[1] W3C Semantic Web Activity <https://www.w3.org/2001/sw/>`_.

