.. _assessment-concepts-guidelines:

Assessment concepts 
===================

The set of Assessment concepts enables the representation of evaluations that take into account a predefined scale, classification, staging or scoring system and their results.

Concept design
--------------

The concept design allows for sharing data about the assessment scale and the assessment result either as code or as 
free text (string) as well as sharing information about the type of person who performed the assessment and the range of the 
assessment scale where applicable.

.. image:: ../images/concepts_guidelines/Assessment_general_design.png
   :width: 90%
   :align: center
   :alt: Assessment concepts design

**Figure 1.** Design of the Assessment data concepts.

Examples for data delivery
--------------------------

The Sequential Organ Failure Assessment (SOFA) and the corresponding result scores(s) can, for example, be instantiated with the assessment concepts. Figure 2 shows an example in which a SOFA total score of 1 is documented for a patient in the intensive care unit. The score was calculated on 3rd January 2023, 1:30 pm by an intensive care specialist.

.. image:: ../images/concepts_guidelines/Assessment_data_delivery1.png
   :width: 90%
   :align: center
   :alt: SOFA total score.

**Figure 2.** Example of a SOFA total score.

Figure 3 shows an example in which a SOFA sub score for the central nervous system of 3 has been documented for a patient in the intensive care unit. The sub score was calculated on 4th March, 4:00 pm by an intensive care specialist.

.. image:: ../images/concepts_guidelines/Assessment_data_delivery2.png
   :width: 90%
   :align: center
   :alt: SOFA sub score.

**Figure 3.** Example of a SOFA sub score.


Guideline for data delivery
---------------------------

**General**

- It is recommended to either request/provide the total score or the single sub scores depending on the granularity needed for the research question.

- SNOMED CT and LOINC feature a broad selection of codes for multiple standardized assessment measures (survey instruments) including staging and scores.

    * For SNOMED CT, the value set of the code-attribute of Assessment includes the hierarchies "Staging and scales", "Procedure", and "Observable entity", and the value set of the code-attribute of Assessment Component includes the hierarchies "Procedure" and "Observable entity".

- The recommended procedure on code usage is as follows:

    * Coding systems: SNOMED CT > LOINC, i.e., use of SNOMED CT is the preferred option if available
    * Use of SNOMED CT hierarchies: Staging and scales > Procedure > Observable entity, i.e., codes from the "Staging and scales" hierarchy are the most preferred option
    * If a code is missing it should be requested as a SNOMED CT code under the "Staging and scales" hierarchy (at  `https://ch-rmp.snomedtools.org/en <https://ch-rmp.snomedtools.org/en>`_).


**Assessment and Assessment Component**

- If SNOMED CT provides a code for the scale (staging system or classification), then the hasCode property should be populated with an instance of SNOMED CT code. Only if there is no SNOMED CT code available the hasName property should be populated.
- Duplicate information should be avoided, i.e., if there is a code available, a name should NOT be provided in addition.


**Range**

- Sometimes it is beneficial to be able to express the range of the scale of an assessment, for example the score on a visual analog scale (VAS). VASs may for example range from 0 to 10 or 0 to 100 and depending on the data provider different scales may be used. A range is therefore helpful to put a result, e.g., 9, into perspective.
- Ideally, range information is implicitly covered by a suitable code for the Assessment or Assessment Component, but in absence of such codes for all scales the range attribute provides an alternative.
- Delivery of the range-attribute is **optional** and the range information may not be available in clinical data warehouses. If desired, it would be up to projects to enrich the knowledge graph accordingly.
- **In case a code for the assessment scale already conveys the information or the respective range is considered common knowledge, the range should not be delivered** to avoid duplication of information. For example, Apgar subscores allow a range of 0 to 2, yet no range should be delivered since this is common knowledge.
- The range information should only be delivered if there is no other means to derive it from the available information.
- The range of an Assessment Component indicates the range of the subscores. The range of the Assessment indicates the range of the total score.
- Note: The SPHN Semantic Working Group has discussed whether links to the range attribute should be added to Assessment and Assessment Component or rather to Assessment Result. In the latter case one would interpret the range as the interval results can potentially fall into. The range, however, refers to the numerical range of the scale of the assessment, not the results. Linking it to the Assessment Result may therefore be misleading as it could be misinterpreted as the range being the actual result (“The result is a range of 0-100.”), in particular as the meaning of range is not as clearly defined as for reference ranges. It has thus been decided to establish the links Assessment to Range and Assessment Component to Range.


**Assessment Result**

- Numeric results should be instantiated via AssessmentResult.hasQuantity (see examples in figure 2 and 3) using the UCUM unit {#} ("number").
- Non-numeric results should be instantiated as code if a code (from SNOMED CT or other resource) is available.
- Only if no code is available for a non-numeric result, AssessmentResult.hasStringValue should be instantiated.
- If the result is a code, such as the SNOMED CT Apgar finding code (for the Apgar score assessment scale), the most specific code reflecting the source data available should be used, e.g. 169901001 | Apgar at 1 minute = 4 (finding) | is preferable over 275307002 | Apgar normal (finding) | (if temporal information is available).
- For an overview of preferred SNOMED CT codes, please check the Assessment concept documentation on the `Git-repository <https://git.dcc.sib.swiss/sphn-semantic-framework/sphn-schema/-/tree/master/dataset>`_ of the SPHN Semantic Interoperability Framework. Please consult the latest version of the `Assessment`- documentation (Addendum: "Scores and Scales in SNOMED CT").


Example for semantic inheritance
--------------------------------

The assessment concept design can be used to create specific assessment concepts that are required, for example, 
if value sets need to be limited (restrictions) or additional information needs to be expressed or linked. The representation 
of the tumor grades is such an example: the Tumor Grade Assessment Event concept has been created inheriting from the 
Assessment Event concept, and the Tumor Grade Assessment concept has been created inheriting from the Assessment concept. 

Figure 4 illustrates the inheritance as well as the associated restriction of the code in this example. Tumor Grade 
Assessment hasCode contains the following value set specification: 
descendant of: SNOMED CT 277457005 |Histological grading systems (staging scale)|, which is more restrictive than the 
value set of the more general concept of Assessment (SNOMED CT and LOINC, with a restriction for SNOMED CT to 
descendant of: 254291000 |Staging and scales (staging scale)|; 
descendant of: 363787002 |Observable entity (observable entity)|; 
descendant of: 71388002 |Procedure (procedure)|).

.. image:: ../images/concepts_guidelines/Assessment_inheritance.png
   :width: 90%
   :align: center
   :alt: Tumor grade representation using Assessment concepts.

**Figure 4.** Example of tumor grade representation based on the Assessment concept design.


