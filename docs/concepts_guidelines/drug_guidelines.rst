.. _drug-concepts-guidelines:


Drug and related concepts
===========================

Concept design
---------------


The concepts Drug, Drug Article, Drug Administration Event, and Drug
Prescription are of significant importance for the representation of
processes concerning medication and treatment. The concepts are defined
as follows:

-  **“Drug”** is defined as “any substance with the intent to prevent, diagnose, treat, or relieve symptoms of a disease or abnormal condition”.
  
-  **“Drug Article”** represents “general details identifying a medication on the level of its commercial article”. Often, several commercial articles of the same drug exist, e.g., in different packaging sizes. Hence, Drug article is not to be confused with a drug on the level of a product.
  

-  **“Drug Prescription”** specifies the “plan that defines at which frequency a drug should be administered to a patient with a given quantity; **at every frequency time point a drug administration event should occur**\ ”.
..

     Notably, the Drug Prescription concept is **limited to a single drug** for technical reasons, unlike for example the prescription paper issued by a doctor. In case multiple drugs are prescribed, multiple Drug Prescriptions need to be instantiated. It is important to emphasize that **one Drug Prescription can trigger multiple Drug Administration Events**.

-  **“Drug Administration Event”** represents a “single event at which a drug was administered to the patient; this could be a single time point in case of a pill/cream or a duration in case of a single infusion pack or a single patch; **one or many drug administration events are initiated by a drug prescription** depending on the frequency stated in the prescription”.
..

   Notably, the Drug Administration Event concept is **limited to a
   single drug** for technical reasons. In case multiple drugs are
   administered, multiple Drug Administration Events need to be
   instantiated.

Note that there is **no direct link between** a **Drug Prescription and** a **Drug Administration Event** as this linkage information is often not available in hospitals. The concepts are linked, however, via Administrative Case, Subject Pseudo Identifier, and Source System.


The active substance is of key importance for a drug administration rather than the drug itself. Therefore the drug prescribed and the drug administered may in practice differ since there are often multiple drugs with the same active substance, e.g., Aspirin, Dolopirin, Alka Seltzer etc. all having acetylsalicylic acid as active ingredient. Compare “Example for data delivery”.

.. image:: ../images/concepts_guidelines/Drug_concept_guideline_01_2025-01-15.png
   :height: 300
   :align: center
   :alt: Drug concepts - overview

**Figure 1:** Overview of the design of Drug-related concepts

Examples for data delivery
--------------------------

The example from Figure 2 shows the hypothetical case of a 78 year old
patient (as of 2024-06-21) who had been diagnosed with a form of
atherosclerotic heart disease (ICD-10-GM code I25.10) several years ago
(on 2022-03-07). The patient gets prescribed ASPIRIN CARDIO 300 mg
(GTIN: 7680517950598) film-coated tablets for prophylactic reasons on
2024-06-21. Note that the information that one tablet contains 300 mg of
acetylsalicylic acid (master data from HCI Compendium) is not explicitly
contained! The patient shall take 2 tablets (non-standard unit {#})
orally 3 times per day, and intake is supposed to start on 2024-06-24
and end on 2024-09-24. The amount of the active ingredient
acetylsalicylic acid (ATC code: B01AC06) per single Drug Administration
Event triggered by the Drug Prescription is intended to be 600 mg (2
tablets with 300mg each per intake). The tablets also contain
methylcellulose (ATC code: A06AC06) as an inactive ingredient.

In the example, the patient uses a drug from a different manufacturer
("ASS CARDIO Zentiva 100 mg", GTIN: 7680686590014) with a different
formulation yet the same active ingredient (acetylsalicylic acid (ATC
code: B01AC06)). The pharmaceutical dose form of the alternative product
are gastro-resistant oral tablets with 100 mg acetylsalicylic acid (ATC
code: B01AC06) per tablet. Again, this information on pharmaceutical strength is not
explicitly represented in the graph! The tablets also contain 60 mg
"Lactose-1-Wasser" per tablet as inactive ingredient (HCI Compendium
master data). The patient takes an oral dose of 6 tablets (each with 100
mg acetylsalicylic acid) on 2024-06-24 at 08:01:30, reaching the
intended total amount of 600 mg per intake.

For each medication (prescribed and administered) several packaging
sizes are available, therefore the GTIN of the smallest packaging size
was used (7680517950598 and 7680686590014, respectively).

Please note that in Figure 2 the connections to Administrative Case, 
Subject Pseudo Identifier, and Source System are
only shown for Drug Prescription and Drug Administration Event for
clarity, and to Source System for Drug and Drug Article.

In this example, Drug, Drug Prescription, and Drug Administration Event
use the same Source System while Drug Article information is stored on
another Source System.

.. image:: ../images/concepts_guidelines/Drug_concept_guideline_02_instantiation_2025-01-15.png
   :height: 400
   :align: center
   :alt: Drug concepts - instantiation example 1

**Figure 2:** Example of mock instantiation of Drug Prescription and
Drug Administration Event where a patient got prescribed a certain
medication (Aspirin Cardio 300mg) and gets administered an alternative
product (ASS CARDIO Zentiva 100 mg) with a different dosage and
formulation, however, receiving the same total amount of active
ingredient. Note that the instances are randomly numbered, e.g., 
"drug prescription 1", "quantity 14", or "substance 4".

Guideline for data delivery
---------------------------

Drug Article
*************

The “Drug Article”-concept provides metainformation on the “Drug”
associated with “Drug Prescription” and “Drug Administration Event”.
This includes a GTIN-code and the pharmaceutical dose form. A name can
also be provided as a string **if no GTIN is available**. Note that
Pharmacodes are not in scope as code standard for the “Drug
Article”-concept as Pharmacodes do neither represent an international
standard nor are they openly available but need to be licensed from HCI
Solutions.

Note that **GTINs do not represent a product code!** Instead, GTINs are
article codes which vary for different packaging sizes of the very same
medication. This may lead to the false conclusion that patients have
been treated with different drugs when only taking into account
differing GTINs. It is therefore **recommended to use the GTIN of the
smallest available packaging size** for Drug article, regardless of the
package size prescribed or the package size the medication has in fact
been administered from.

Please note that the current Drug-related concepts (release 2024.2) do
not allow to represent the “drug strength” of a drug (NCIT code
`C53294 <http://nciws-p803.nci.nih.gov:9080/ncitbrowser/ConceptReport.jsp?dictionary=NCI_Thesaurus&ns=ncit&code=C53294>`__
“pharmaceutical strength”: “The content of an active ingredient
expressed quantitatively per dosage unit, per unit of volume, or per
unit of weight, according to the pharmaceutical dose form.”). Such
master data is currently only available to the data providers which have
licensed this information from HCI solutions. Notably, once metadata
would become available which features normalized names and unique
identifiers for medicines and drugs like for example
`RxNorm <https://www.nlm.nih.gov/research/umls/rxnorm/overview.html>`__
it could be readily connected to Drug Article. Such a terminology would
also resolve the issues associated with different GTINs for different
packaging sizes.

Quantities
**********

Semantically, attributes refer to the parent concept, therefore a 
“quantity”-attribute has context-dependent meanings:

-  Since release 2024.1, Drug Prescription and Drug Administration Event do not hold a “quantity”-attribute any more as this has repeatedly led to confusion about what information to provide there. A “quantity” of “Drug Prescription” and “Drug Administration Event” would indicate the number of drug prescriptions or drug administrations, not an amount of drug prescribed or administered.


-  A **“quantity”-attribute of “Drug”** refers to the drug amount, e.g., 3 tablets or 5 mL.

-  A **“quantity”-attribute of “Substance” (as attribute of “Drug”)** refers to the amount of active or inactive components within the drug, e.g., 500 mg or 20 IU. This quantity does NOT represent the amount of active or inactive ingredient per dosage form ("drug strength") but the amount of active or inactive ingredient for the amount of drug as specified by the "quantity"-attribute of Drug!

For multiple drug prescriptions, multiple instances of “Drug
Prescription” shall be instantiated. A Drug Administration Event
represents by its definition (see above) a “single event at which a drug
was administered”, i.e., a quantity-attribute is not meaningful. As for
Drug Prescription, multiple drug administration events shall be
represented by multiple instances of “Drug Administration Event”.

Along with the removal of the “quantity”-attribute from Drug
Prescription and Drug Administration Event a “quantity”-attribute has
been introduced in the “Drug”-concept, in line with the design of the
“Substance”-concept. Notably, the quantity of the drug is not to be
confused with the quantity of the active or inactive substances
contained in the drug.

Following the concept definitions, the composedOf ‘quantity’ of the Drug
concept represents the following different aspects in the context of
Drug Prescription and Drug Administration Event:

-  **Drug Prescription:** ‘quantity’ of the Drug represents the **quantity intended** to be **administered during a single Drug Administration Event.** It corresponds to the quantity prescribed or written in the Drug Prescription, e.g. “2 tablets” if “2 tablets 3 times per day” are prescribed: “2 tablets” is covered by the quantity of the Drug, “3 times a day” by the frequency of the Drug Prescription.

-  **Drug Administration Event:** The ‘quantity’-attribute of the Drug represents the **total quantity of a drug administered during a single Drug Administration Event**, e.g., 2 tablets between start and end datetime, or 500 ml during the duration.

..

   Note: The ‘quantity’-attributes of the active and inactive
   ingredients of Drug represent the total amount of the specific
   ingredient administered during the Drug Administration Event.


.. _drug-concepts-units:
Units
******

The unit of the “quantity”-attribute of Drug shall be represented in
UCUM units whenever possible, e.g., mL or mg.

Non-standard units of drugs, however, cannot be represented properly,
e.g., drops, sachets, tablets, pills, or ampoules etc. In such cases the
symbol {#} is to be used as a unit. The label of “{#}” is “number”.
Information on the pharmaceutical dose form can be retrieved via the
“pharmaceutical dose form”-attribute of the Drug Article associated with
the Drug-instance.

The unit of the “quantity”-attribute of active or inactive substances
associated with a drug shall be represented in UCUM units like mg or IU.

Total quantities
****************

Drug amount
!!!!!!!!!!!!

Total quantities of Drug over a specific period can (and need to) be
calculated, either including all Drug Administration Events during the
time period of interest, or spanning the time between first and last
administration datetime for Drug Prescription.

*Comment: Formulas to be inserted as rendered text or graphics in the
version for Readthedocs.*

The total amount of drug (e.g., 6 tablets or 2 mL etc.) **administered over a
period of time** involving multiple identical Drug Administration Events (DAEs)
is calculated as follows:

..
  .. image:: ../images/concepts_guidelines/Drug_Eq_01_total_amount_of_drug_administered_over_a_period_of_time_involving_multiple_identical_Drug_Administration_Event.png
     :height: 30

.. math::
  total \hspace{0.5ex} & amount_{drug \hspace{0.5ex} administered} = \\[2pt]
    & quantity_{Drug}  \\[2pt]
    & \times (number \hspace{0.5ex} of \hspace{0.5ex} DAEs \hspace{0.5ex} 
    during \hspace{0.5ex} period \hspace{0.5ex} of \hspace{0.5ex} interest)
  
Notably, this is **not to be confused with the total amount of active or
inactive ingredients!** See section “(Active) Ingredients” below for
further details.


For a **single Drug Administration Event** this therefore simplifies to:

..
  .. image:: ../images/concepts_guidelines/Drug_Eq_02_total_amount_of_drug_administered_for_a_single_Drug_Administration_Event.png
     :height: 30

.. math::
  total \hspace{0.5ex} & amount _{drug \hspace{0.5ex} administered} \hspace{0.5ex} \\[2pt]
    & = \hspace{0.5ex} quantity_{Drug} \hspace{0.5ex} \times \hspace{0.5ex} 1 \\[2pt]
    & = \hspace{0.5ex} quantity_{Drug}


The total amount of drug **prescribed over a period of time** is
calculated as follows (assuming full days for the administration
datetimes):

..
  .. image:: ../images/concepts_guidelines/Drug_Eq_03_total_amount_of_drug_prescribed_over_a_period_of_time_assuming_full_days_for_the_administration_datetimes.png
     :height: 30

.. math::
  total & \ amount_{drug \ prescribed} \\[2pt]
    & = \ quantity_{Drug} \\[2pt]
    & \hspace{3ex} \times \ frequency_{Drug \ Prescription} \\[2pt]
    & \hspace{3ex} \times \ (last \ administration \ datetime \\
    & \hspace{7ex} - \ first \ administration \ datetime \ +  \ 1)_{Drug \ Prescription}



(Active) Ingredients
!!!!!!!!!!!!!!!!!!!!!

In the context of Drug Administration Events, not only the amount of
drug but also the **total amount of active ingredients(s)** is of
particular interest. For a **single Drug Administration Event** it is
calculated as follows:

..
  .. image:: ../images/concepts_guidelines/Drug_Eq_04_total_amount_of_active_ingredients(s)_for_a_single_Drug_Administration_Event.png
     :height: 30

  .. math::
    total \hspace{0.5ex} amount_{active \hspace{0.5ex} ingredient \hspace{0.5ex} administered} \hspace{0.5ex} =
      \hspace{0.5ex} \frac{amount \hspace{0.5ex} active \hspace{0.5ex} ingredient}{dosage \hspace{0.5ex} form}
      \hspace{0.5ex} \times \hspace{0.5ex} quantity_{Drug}

.. math::
  total \ amount_{active \ ingredient \ administered} \ =
  \ \frac{amount \ active \ ingredient}{dosage \ form}
  \ \times \ quantity_{Drug}



**The total amount is to be stored under the ‘quantity’ of the
respective active ingredient of the Drug** as attributes are
context-dependent and describe the instance of the concept they refer
to.

Notably, **this amount needs to be delivered pre-calculated by the data
provider** as the RDF schema (release 2024.2) holds no place to store
the amount of active ingredient per dosage form, i.e., the pharmaceutical strength (e.g., amount of active
ingredient per 1 tablet or 1 ml).

**Notably, the total quantities of different active or inactive
ingredients must NOT be summed up!** Such a sum holds no meaningful
information.

The **total amount of active ingredient** for multiple identical Drug
Administration Events (DAEs) **over a specific period of interest** is often of
particular clinical interest, e.g., in cancer therapy, and calculated as
follows:

..
  .. image:: ../images/concepts_guidelines/Drug_Eq_05_total_amount_of_active_ingredient_for_multiple_identical_Drug_Administration_Events_over_a_specific_period_of_interest.png
     :height: 30

.. math::
  total \hspace{0.5ex} & amount_{active \hspace{0.5ex} ingredient \hspace{0.5ex} administered} \\[5pt]
  & = \hspace{0.5ex} \frac{amount \hspace{0.5ex} active \hspace{0.5ex} ingredient}{dosage \hspace{0.5ex} form} \\[2 pt]
  & \hspace{4ex} \times \hspace{0.5ex} quantity_{Drug} \hspace{0.5ex} \\[2pt]
  & \hspace{4ex} \times \hspace{0.5ex}  (number \hspace{0.5ex} of \hspace{0.5ex} DAEs \hspace{0.5ex} during \hspace{0.5ex} period \hspace{0.5ex} of \hspace{0.5ex} interest)



Limitations and caveats
************************

Several specific use cases cannot currently be fully detailed, in part
because an openly available terminology of drug metadata is missing.

-  **Non-standard units of drugs, e.g., drops, sachets, ampoules etc, need to be represented using {#} as a unit, see section "Units" above.**
  
  
-  **Non-standard units like “drop” would need to be represented in SI-units for full transparency,** e.g., 1 drop = 0.036 ml.

..

   Example: GANFORT Gtt Opht (eye-drops for glaucoma treatment; GTIN
   #7680576300013 or 7680576300037).

   This information is available in the HCI compendium (which needs to
   be licensed) or directly from the drug manufacturer but is not
   allowed to be represented in the current SPHN schema. Unfortunately,
   this undermines efforts to provide all elements for the calculation
   of amounts of active ingredients for a specific dose.

   The representation of such a drug dosage equivalence involves a
   **calculation** which **needs to be carried out by the data
   provider** when retrieving the drug amount but is out of scope of the
   current SPHN concepts.

-  **For some drugs the reference amount is not a single unit.**

..

   Example: SPRAVATO Nasenspray 28 mg (antidepressant nasal spray; GTIN
   # 7680671030013, 7680671030020, or 7680671030037) where a dose of 28
   mg Esketamin is contained in 2 puffs (presumably one puff per nostril
   intended). Such a reference amount could be represented in an
   extended ‘Drug Article’-concept in the future but is out of scope of
   the current SPHN concepts.

-  **Some drugs are not applied “as is” but need to be pre-processed**, for example, dissolved.

..

   Example: Fibrogammin 1'250 I.E.(GTIN # 7680006710061) containing
   1’250 I.U. dried coagulation factor XIII (human) is to be dissolved
   in 20 ml water for injection or infusion. Without this reference
   amount information, the information that a specific volume was
   administered is not informative.

   In this particular case, dissolving the drug represents a processing
   step which cannot be represented using the current SPHN concepts.
   **The calculation of the administered quantity of active substance(s)
   needs to be carried out by the data provider.**

-  **Some drugs need to be diluted before use to reach a recommended concentration.** This involves both a processing step and a calculation which again is out of scope of the current SPHN concepts. **Corresponding calculations need to be carried out by the data provider when retrieving the quantity of active substance(s).**

..

   Example: of LAVASEPT Konz (GTIN # 7680504450704; containing 200 mg/ml
   Polihexanidum) which is recommended to be diluted to a final
   concentration of 0.2 to 0.4 mg/ml Polihexanidum before being
   administered.
