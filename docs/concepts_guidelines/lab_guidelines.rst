.. _lab-concepts-guidelines:

Lab concepts 
=============

The set of Lab and Microbiology concepts enables the representation of different kinds of laboratory tests performed on a sample.

Concept design
--------------

The general concept Lab Test Event aims to specify for a given sample, the different tests performed on it and their associated results. The design is structured with 3 main concepts (see Figure 1):

- The Lab Test Event which represents the process of laboratory tests performed on a sample at a given time point. A Lab Test Event can connect to one to many Lab Test(s) being performed on that sample. 
The Lab Test Event is the main concept of the laboratory tests which directly connects to the Source System, the Administrative Case and the Subject Pseudo Identifier.
- The Lab Test which covers a specific lab test performed on the sample specified in the Lab Test Event concept.
- The Lab Result holds the outcome of a specific lab test. 
The result can be either coded (Code), a quantitative value and unit (Quantity) or a string value. 
The result also holds the Reference Range or Reference Value that complies with the specific sample being tested in a specific laboratory test with defined parameters and with a specific result obtained. 
All these information are taken into account with the reference set for a Lab Result.

.. image:: ../images/concepts_guidelines/Lab.png
   :height: 200
   :align: center
   :alt: Lab

**Figure 1.** Design of the general laboratory tests data concepts.

Figure 1 additionally shows how a Lab Result links to a Reference Interpretation. A Lab Result holds a particular outcome and defined reference, both are taken into account in an interpretation process where the input is the Lab Result and the output is a specific SNOMED CT code interpreting the combination of the result and its reference. 

From this general design of laboratory tests, specific microbiology laboratory tests have been conceptualised (see Figure 2). For each of these different kinds of tests, the design of the concepts is the same: a test event is defined, which holds tests, which in turn holds a result. For instance, for microorganism identification, there is a Microorganism Identification Lab Test Event, a Microorganism Identification Lab Test and a Microorganism Identification Result.

.. image:: ../images/concepts_guidelines/Microbiology.png
   :height: 200
   :align: center
   :alt: Lab

**Figure 2.** Hierarchical relation between the general laboratory concepts and their inherited ‘specialised’ microbiology concepts.

The following four groups of microbiology laboratory tests are available:

(1) **Microorganism Identification**: covers laboratory tests where a sample is usually cultured to identify microorganisms present in the culture environment. The Microorganism Identification Result specifies the Organism detected in the sample tested. Thus, one Microorganism Identification Test can hold multiple results, each identifying a different organism.

.. image:: ../images/concepts_guidelines/MicroorganismIdentification.png
   :height: 200
   :align: center
   :alt: Microorganism identification

**Figure 3.** Design of the Microorganism Identification data concepts.

(2) **Antimicrobial Susceptibility**: covers susceptibility and resistance tests performed on isolates (in which the single organism present is known) against substances or drugs to detect resistance or susceptibility. The isolate is specified in the Antimicrobial Susceptibility Lab Test Event. The Substance or Drug against which the susceptibility test is performed is specified in the Antimicrobial Susceptibility Lab Test. An Antimicrobial Susceptibility Result is specific to an isolate being tested against one specific Drug or Substance.

.. image:: ../images/concepts_guidelines/AntimicrobialSusceptibility.png
   :height: 200
   :align: center
   :alt: Antimicrobial susceptibility

**Figure 4.** Design of the Antimicrobial Susceptibility data concepts.

(3) **Microbiology Microscopy**: covers microscopy laboratory tests performed on samples or isolates where cells may be observed through their structure and shapes. The Microbiology Microscopy Result contains more contextualised information than the general Lab Result concept such as cell morphology, cell organisation.

.. image:: ../images/concepts_guidelines/MicrobiologyMicroscopy.png
   :height: 200
   :align: center
   :alt: Microbiology microscopy

**Figure 5.** Design of the Microbiology Microscopy data concepts.

(4) **Microbiology Biomolecule Presence**: covers laboratory tests done to detect and quantify the presence of a molecule. The Microbiology Biomolecule Presence Lab Test specifies the organic compound being analysed (Gene or Protein in this case) and the Microbiology Biomolecule Result contains the result for that specific compound.

.. image:: ../images/concepts_guidelines/MicrobiologyBiomolecule.png
   :height: 200
   :align: center
   :alt: Microbiology biomolecule presence

**Figure 6.** Design of the Microbiology Biomolecule Presence data concepts.

Examples for data delivery
--------------------------

The first example from Figure 8 shows a general lab test event performed on a patient to check the levels of Sodium in plasma. A plasma sample is taken from the patient’s forearm and tested with a Cobas instrument. The result obtained (165mmol/L) is compared with the reference range (135-145 mmol/L) annotated for this patient’s characteristics and interpreted as being above the reference range. This example shows how the different information can be instantiated and put together.

.. image:: ../images/concepts_guidelines/Lab_Instantiation.png
   :height: 200
   :align: center
   :alt: Laboratory test instantiation example

**Figure 7.** Example of mock instantiation of a general Lab Test Event where the sample of a patient got a basic metabolic test done (check of Sodium in Plasma).

The second example from Figure 9 shows a sample (sample1) which is first placed onto a petri dish for the purpose of culturing bacteria, aiming to identify the microorganisms present in the environment. This is depicted with the microorganismidentificationlabtestevent1 where two organisms are being identified (E. Coli and G. Klebsiella). The sample is then processed to isolate these two organisms which results in two different isolates that each basically represent the sample combined with the isolated organism (instance of sampleprocessing1). The instance of  sampleprocessing1 shows the process in which from sample1 we obtain isolate1 and isolate2. These isolates are then separately put onto a susceptibility test event through disk diffusion to check for their resistance or susceptibility to particular substances. E. Coli is resistant while G. Klebsiella is susceptible in this example.

The connection between a sample and its isolated isolates is with the sample processing concept.

Please note that for clarity purposes, we avoided in figure 8 representing the connections to the Administrative Case, the Subject Pseudo Identifier and the Source System, as well as Reference Ranges, Reference Interpretation and other metadata.

.. image:: ../images/concepts_guidelines/Identification_Susceptibility_Instantiation.png
   :height: 200
   :align: center
   :alt: Microorganism identification antimicrobial susceptibility test instantiation example

**Figure 8.** Example of mock instantiation of a Microorganism Identification Lab Test Event combined with an Antimicrobial Susceptibility Lab Test Event.

Guideline for data delivery
---------------------------

**General**

- One instance of a Lab Test Event holds an instance of a Sample. This instance of Lab Test Event can have one to many instances of Lab Tests being performed. Each instance of these Lab Tests can have one to many instances of Lab Results. This applies to microbiology-specific concepts.
- The Lab Result and microbiology specific Results concepts are direct descendants of the Result concept. Each of the specific result concepts are tailored to be used in their particular context.

**Reference Range and Reference Value**

- The reference whether it is a range or value for a given lab test is not connected to the Lab Test directly. It is bound to a Lab Result to take into consideration specific context needed for associating a reference information to the combination of a Sample, Lab Test and Lab Result. For instance, the Reference Value for a microorganism identification test depends on the Organism identified, which is connected in the SPHN Dataset, to the Microorganism Identification Result (see Figure 3).

**Reference Interpretation**

- An instance of Lab Result given as input of a Reference Interpretation instance must be the same as an instance of Lab Result describing the result of a Lab Test. This is the way that data comes together (a result with its interpretation).

**Sample and Isolate**

- The Isolate is a descendant of Sample and contains an additional composedOf (in comparison to the Sample concept), which specifies the Organism associated to this Isolate.
- The process in which a Sample is split into, possibly, one to many Isolate(s) can be encoded with the Sample Processing concept (see Figure 9).
- An Antimicrobial Susceptibility Lab Test Event and a Microbiology Biomolecule Presence Lab Test Event can only be done on an Isolate.

.. image:: ../images/concepts_guidelines/SampleProcessing.png
   :height: 200
   :align: center
   :alt: Microorganism identification antimicrobial susceptibility test instantiation example

**Figure 9.** Sample Processing concept. When extracting Isolates from a Sample, the input would be an instance of a Sample and the output would be an instance of an Isolate.






