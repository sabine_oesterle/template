.. _measurement-concepts-guidelines:

Measurement concepts 
=====================

The set of Measurement concepts enables the representation of measurements done on a patient with their outcome (measurement result). The measurement process and its resulting result are modelled as two distinct concepts.

Concept design
--------------

The concept design allows for sharing data about different types of measurement and their result. The Measurement can contain metadata about:

- the performer of the measurement
- the start and end datetime of the measurement
- the (code of the) method used to do the measurement
- the medical device used to do the measurement

The Measurement connects to a specific Result which is mostly a quantitative (Quantity) element. Nevertheless, Code and free text values can be used for encoding the result of a measurement, if applicable.

The Measurement directly connects to the Source System, Administrative Case and Subject Pseudo Identifier. 
The Result is not intended to be directly connected to these three main concepts.
This means, an instance of a result must always be created in the context of another concept (here, a measurement).

.. image:: ../images/concepts_guidelines/Measurement.png
   :height: 200
   :align: center
   :alt: Measurement concepts

**Figure 1.** Design of measurement data concepts.

The Measurement concept is designed with specific measurement types being defined and which should be instantiated for representing data of a particular measurement (e.g. Body Height Measurement, Heart Rate Measurement, Oxygen Saturation Measurement). These specific measurement concepts are descendants (or children) of the Measurement concept. The outcome of these measurements follow the same pattern and have specific Result concepts being defined for the instantiation of data representing particular measurement results (e.g Body Height, Heart Rate, Oxygen Saturation). A particular measurement and its result should always be used in combination (see Figure 2). For example, an instance of a Cardiac Output Measurement must have as a result an instance of a Cardiac Output.

.. image:: ../images/concepts_guidelines/Measurements_List.png
   :height: 300
   :align: center
   :alt: Measurements list

**Figure 2.** List of SPHN 2024.1 Measurement and their Result concepts.


Examples for data delivery
--------------------------

The height of the patient being measured in a clinical setting results in the instantiation of a Body Height Measurement concept and a Body Height (which inherits from Result) concept (see Figure 3).

.. image:: ../images/concepts_guidelines/BodyHeight_Instantiation.png
   :height: 200
   :align: center
   :alt: BodyHeight instantation

**Figure 3.** Example of mock instantiation of a Body Height Measurement and its Body Height result.

Another example is about a Heart Rate Measurement of a patient. The patient was auscultated and three values were measured. The example shows how results can be provided in the data: either directly instantiated in the graph (a) or given in a separate file than the graph, using the Time Series Data File (b) (see below for more information about the Time Series Data File).

.. image:: ../images/concepts_guidelines/HeartRate_Instantiation.png
   :height: 600
   :align: center
   :alt: BodyHeight instantation

**Figure 4.** Example of Heart Rate Measurement where (a) values are instantiated in the graph and (b) values are provided in a Time Series Data File.

Guideline for data delivery
---------------------------

**General**

- The Measurement concept must never be instantiated. Instead, a specific measurement concept (e.g. Body Height Measurement, Heart Rate Measurement) must always be used and instantiated for encoding specific measurements. If the needed measurement concept is missing, a project can define it in the project-specific schema.

**Time Series Data File**

A few measurement concepts can include Time Series Data File (Blood Pressure Measurement, Body Temperature Measurement, Cardiac Output Measurement, Heart Rate Measurement, Oxygen Saturation Measurement, Respiratory Rate Measurement).

1- The recommendation is to always use the ‘Result concept’ - hasQuantity to explicitly encode the numeric results (measurement values) in the RDF graph. Nevertheless for data measured in automated way in a time series data (e.g. with sensors), which include numerous data points in a single measurement event (e.g. ICU data), the values can be encoded and provided in a separate file, which would be referred to in the ‘Result concept’ - hasDataFile. This data file is a Time Series Data File instance which would contain metadata about the file containing the values. That means, **one** instance of Time Series Data File can connect to **one** instance of Measurement as a Result. 

2- It is not recommended for a given set of measurement concepts (Measurement + its Result) to mix results provided as values in the graph (Quantity) and results provided in a data file (Time Series Data File). A SPHN project should evaluate and must state to data providers if data should be sent for a particular measurement as a Time Series Data File or directly in the graph as Quantity (see Figure 4) while keeping in mind the recommendations stated in point 1.