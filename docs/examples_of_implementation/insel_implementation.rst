Insel implementation of SPHN
############################

The Insel Group has developed fundamental architecture principals. These
principals guide the several projects running at the Insel. Important in this
context is that the Insel Data Platform has a clear position in the Insel
Enterprise Architecture. In the following, we present the important
fundamental principles regarding our Insel Data Platform:

Fundamental principles: Central Data Analysis:

The Insel Data Platform (IDP) is the central hub for data access and
subsequent analysis

1) IDP collects, consolidates and aggregates clinical and administrative data
   (structured + unstructured) from the entire Insel Group.
2) IDP supports strategic and tactical management of the Insel Group.
3) IDP supports the Insel Group's researchers with data.
4) IDP supports the development of Proof of Concepts for Clinical Decision Support.
5) IDP exchanges data with third parties (e.g. registry connections, but not EPD).
6) IDP consists of multiple technology stacks (e.g. SAP-BW, Epic Caboodle, Epic
   Clarity, SQL, Hadoop clusters) in which the data are gathered and integrated.
7) Clinical and administrative data from legacy systems will be transferred to
   IDP for the above listed use cases, but not for legal cases.
8) IDP is not a legally compliant archiving system.


The figure below shows the overall picture of the Insel Data Platform and how
the different topics are linked together:


.. image:: ../images/insel/overview.png
   :align: center
   :alt: Insel Data Platform

**Figure 1:** Overview of the Insel Data Platform and Interconnections between Key Components


The Insel Data Platform consists of the following components, which each of
them have its own purpose:

- Enterprise data warehouse (currently cDWH / Caboodle, in future only Caboodle)
    - DWH for structural, administrative and clinical data for the purpose of
      analysis and data provision

- SAP BW 
    - an analysis tool used for the ad-hoc analysis of economic and
      administrative ques-tions
    - financial planning tool

- Data Lakehouse (a common platform for applications and data)
    - combines data storage with a processing infrastructure (e.g. NLP execution)
    - storing, analyzing unstructured data like text, DICOM, OMICS, …
    - analyses that require the combination of structured and unstructured data

The following figure shows how the SPHN components are integrated in the IDP:

.. image:: ../images/insel/sphn_integration.png
   :align: center
   :alt: Integration of the SPHN component into the IDP

**Figure 2:** Integration of the SPHN component into the IDP

**Remarks:**

- Only data in accordance to the data governance requirements (HFG, DSG)
  are transferred to BioMedIT 
- The DEAS system allows feasibility analyses so that no patient data can
  leave the Insel Gruppe AG, but cohort building across all UH is possible.
