USZ implementation of SPHN
============================


The research data ecosystem at the University Hospital Zurich (USZ) 
leverages the toolstack of the SPHN Interoperability Framework, 
integrating several key components for efficient data management and processing (see Figure 1). 

.. image:: ../images/usz/usz_research_ecosystem.png
   :align: center
   :alt: USZ research ecosystem
Figure 1. Overview of the USZ research ecosystem



The workflow involves:

Data Aggregation
----------------

Clinical data is extracted from the hospital's clinical data platform and aggregated in a Research Integration Database. 
This database employs a relational model closely aligned with the SPHN Connector's PostgreSQL database schema.

ONTOCAT
-------

Developed in-house at USZ, ONTOCAT manages catalogs, terminologies, value sets, 
and mappings between source data and terminologies, ensuring consistent data annotation and integration.

DeidMaster
----------

The DeidMaster database handles and stores mappings between hospital-specific IDs 
and project-specific Research IDs, including date shifts, to ensure the de-identification of the patient data.

Data Transformation and De-Identification
------------------------------------------

Database views translate source data using ONTOCAT’s mappings, 
de-identify it with information from DeIDMaster, and prepare the data for further processing in the SPHN Connector.

SPHN Connector
--------------

The SPHN Connector transforms relational data from the clinical platform into a graph model, 
which is then stored in the central component of the ecosystem, Einstein.

Einstein and Federated Query
---------------------------- 
Einstein, the large graph store, plays a pivotal role, providing data that is accessible through FastAPI interfaces. 
This enables other services, such as the federated query and analytics system DEAS/TuneInsight, to query the data. 
Additionally, service connectors export data into desired formats, with FHIR being a key priority, 
and store the output on an S3 object storage. 
Data is then securely transferred to researchers via the SETT application.


The core components - SPHN Connector, Einstein, and TuneInsight - 
provide a robust foundation for researchers' data needs. 
To reduce the maintenance efforts and professionalize our environments, 
we decided to bring these important components to VMWare Tanzu, 
our on-premise Kubernetes infrastructure. 
The whole ecosystem is structured into a STAGE and PROD environment depicted in Figure 2 below.


.. image:: ../images/usz/usz_research_ecosystem_environments.png
   :align: center
   :alt: USZ research ecosystem environment

Figure 2. Overview of the STAGE and PROD environment in USZ