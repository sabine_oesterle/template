.. _external-terminologies-naming-conventions:

Naming conventions for terminologies not in RDF
================================================

Versions of external terminologies used in the context of the SPHN infrastructure and that are not provided in RDF are uniquely identified with a ``sphn:Code`` where the composedOf ``sphn:hasCodingSystemAndVersion``
should be filled with the terminology short name and the version number of the terminology. 
This code should be used whenever it is necessary to identify the exact version of the terminology being referenced. This applies to external terminologies which are not provided in RDF in the context of SPHN.

.. list-table:: Naming conventions for coding systems and versions
   :widths: 15 40 25 20 
   :header-rows: 1

   * - short name
     - full name
     - coding system and version
     - examples
   * - `ClinVar <https://www.ncbi.nlm.nih.gov/clinvar/>`_
     - ClinVar - ClinGen | Clinical Genome Resource
     - ClinVar-[YEAR-MONTH]
     - Clinvar-2022-10
   * - `EDQM <https://www.edqm.eu/en/standard-terms-database>`_
     - European Directorate of the Quality of Medicines & Health Care Standard Terms
     - EDQM-[VERSION]
     - EDQM-1.2.0
   * - `Ensembl <https://www.ensembl.org/index.html>`_
     - Ensembl genome database project
     - Ensembl-[VERSION]-[YEAR-MONTH]
     - Ensembl-108-2022-10
   * - `GMDN <https://www.gmdnagency.org/>`_
     - Global Medical Device Nomenclature    
     - GMDN
     - GMDN
   * - `GTIN <https://www.gs1.org/standards/id-keys/gtin>`_
     - Global Trace Item Number
     - GTIN
     - GTIN
   * - `GUDID <https://www.fda.gov/medical-devices/unique-device-identification-system-udi-system/global-unique-device-identification-database-gudid>`_
     - Global Unique Device Identification Database
     - GUDID
     - GUDID
   * - `HGVS <https://varnomen.hgvs.org/>`_
     - Human Genome Variation Society notation
     - HGVS-[VERSION]
     - HGVS-20.05
   * - `ICD-O-3 <https://www.who.int/standards/classifications/other-classifications/international-classification-of-diseases-for-oncology>`_
     - International Classification of Diseases for Oncology
     - ICD-O-3.[REVISION]
     - ICD-O-3.0; ICD-O-3.1; ICD-O-3.2
   * - `ICPC <https://www.who.int/standards/classifications/other-classifications/international-classification-of-primary-care>`_
     - International Classification of Primary Care
     - ICPC-[VERSION]-[YEAR]
     - ICPC-2-2003
   * - `ISCN <https://iscn.karger.com/>`_
     - International System for Human Cytogenomic Nomenclature
     - ISCN-[YEAR]
     - ISCN-2022
   * - `L4CHLAB <https://sphn.ch/network/projects-old/infrastructure-development-projects/project-page-l4chlab/>`_
     - LOINC for Swiss Laboratories
     - L4CHLAB-[YEAR].[#]
     - L4CHLAB-2019.1
   * - `MedDRA <https://www.meddra.org/>`_
     - Medical Dictionary for Regulatory Activities
     - MedDRA-[VERSION]
     - MedDRA-23.1
   * - `NANDA <https://nurseslabs.com/nursing-diagnosis/>`_
     - NANDA Nursing Diagnoses
     - NANDA-[YEAR-YEAR]
     - NANDA-2018-2020
   * - `NCBI GenBank <https://www.ncbi.nlm.nih.gov/genbank/>`_
     - NCBI GenBank Database
     - NCBI-GenBank-[VERSION]-[YEAR-MONTH-DAY]
     - NCBI-GenBank-251-2022-08-15
   * - `NCBI Gene <https://www.ncbi.nlm.nih.gov/gene/>`_
     - NCBI Gene Database
     - NCBI-Gene-[YEAR-MONTH-DAY]
     - NCBI-Gene-2022-11-22
   * - `NCBI Taxon <https://www.ncbi.nlm.nih.gov/taxonomy>`_
     - NCBI Taxonomy Database
     - NCBI-Taxon
     - NCBI-Taxon
   * - `RefSNP <https://www.ncbi.nlm.nih.gov/snp/>`_
     - NCBI dbSNP Reference SNP catalog
     - RefSNP-[YEAR-MONTH]
     - RefSNP-2022-04
   * - `SPDI <https://www.ncbi.nlm.nih.gov/pmc/articles/PMC7523648/>`_
     - Sequence Position Deletion and Insertion notation
     - SPDI
     - SPDI
   * - `UID <https://www.bfs.admin.ch/bfs/en/home/registers/enterprise-register/enterprise-identification.html>`_
     - Swiss unique enterprise identification number
     - UID
     - UID
   * - `UniProtKB <https://www.uniprot.org/>`_
     - UniProt Knowledgebase
     - UniProtKB-[YEAR-MONTH]
     - UniProtKB-2022-04
