.. _ext_term_versions_used_in_sphn:

Versions used in SPHN
=======================

This page describes which versions of the external terminologies provided in RDF are used in the different versions of the SPHN RDF Schema.


With each release of the SPHN RDF Schema, newer versions of the different terminologies are imported and used. 
Thus, data must be compatible with the versions of external terminologies imported as RDF into the SPHN RDF Schema for the codes to be recognised and identified by the triplestores.
If the data provider has a coded value coming from a version that not compatible with the imported RDF external terminology in the SPHN RDF Schema, then that coded value may be provided using the ``sphn:Code`` class instead.


The following table indicates which versions of the external terminologies (provided as RDF files) can be used in each version of the SPHN RDF Schema.
In other words, it indicates which versions of an external terminology are imported in the different releases of the SPHN RDF Schema.

.. list-table:: External terminologies versions loaded in the three most recent versions of the SPHN RDF Schema
   :widths: 10 30 30 30
   :header-rows: 1

   * - Terminology
     - Schema 2025.1
     - Schema 2024.2
     - Schema 2023.2
   * - ATC
     - 2016 to 2025
     - 2016 to 2024
     - 2016 to 2023
   * - CHOP
     - 2013 to 2025
     - 2013 to 2024
     - 2016 to 2023
   * - ECO
     - 20240719
     - 2023-09-03
     - 
   * - EDAM
     - 1.25
     - 1.25
     - 
   * - EFO
     - v3.72.0
     - v3.61.0
     - 
   * - EMDN
     - 1.1 (2021-9-29)
     - 1.1 (2021-9-29)
     - 
   * - GENO
     - 2023-10-08
     - 2023-10-08
     - 
   * - GenEpiO
     - 2023-08-19
     - 2023-08-19
     - 
   * - HGNC
     - 20241210
     - 20231215
     - 20221107
   * - ICD-10-GM
     - 2013 to 2025
     - 2013 to 2024
     - 2015 to 2023  
   * - ICD-O
     - ICD-O-3 version 2
     - 
     -  
   * - LOINC
     - 2.78
     - 2.76
     - 2.74
   * - OBI
     - 2024-10-25
     - 2023-09-20
     - 
   * - OncoTree
     - 2021-11-02
     -
     -
   * - ORDO
     - 4.6
     - 4.4
     - 
   * - SNOMED CT
     - Dec 2024 (Int + CH edition)
     - Dec 2023 (Int + CH edition)
     - Dec 2022 (International edition)
   * - SO
     - 2021-11-22   
     - 2021-11-22
     - 2021-11-22   
   * - UCUM
     - 2025.1
     - 2024.1
     - 2023.1
   

.. note::
    1. Some of the SPHN RDF Schema versions support multiple versions of a terminology: ATC, CHOP and ICD-10-GM. 
       This is due to the fact that a versioning strategy has been applied which includes also historical codes in the latest version of the terminology (see :ref:`versioning-of-terminologies`).

    2. An empty cell indicate that the external terminology is not supported as RDF in that version of the SPHN RDF Schema.

    3. UCUM is not provided with a specific version since it is a notation. Therefore, the version indicated corresponds to the versionIRI provided by the SPHN UCUM RDF files in the :ref:`framework-terminology-service`.



.. _namespace-external-resources:

Namespace of terminologies used in SPHN
---------------------------------------

The section below highlights the different namespaces (ontology and version IRIs) 
given to the external terminologies provided in the 
:ref:`framework-terminology-service` as an RDF file. In addition, the IRI used for encoding 
the actual codes are also expressed in the section **IRI for codes**. 
Note that these are the IRIs that must be used by the data providers for encoding/annotating coded values of these terminologies.
The version of the terminology being used in the different SPHN RDF schemas is 
highlighted in the **Version IRI**.


ATC namespace
*************

**Ontology IRI**:
:code:`https://biomedit.ch/rdf/sphn-resource/atc/`

**Version IRI example**:
:code:`https://biomedit.ch/rdf/sphn-resource/atc/2023/1` 


**IRI for codes**: 
:code:`https://www.whocc.no/atc_ddd_index/?code=`


CHOP namespace
**************

**Ontology IRI**:
:code:`https://biomedit.ch/rdf/sphn-resource/chop/`

**Version IRI example**:
:code:`https://biomedit.ch/rdf/sphn-resource/chop/2023/4`

**IRI for codes**: 
:code:`https://biomedit.ch/rdf/sphn-resource/chop/`


ECO namespace
**************

**Ontology IRI**:  
:code:`http://purl.obolibrary.org/obo/eco.owl`

**Version IRI example**:
:code:`http://purl.obolibrary.org/obo/eco/releases/2023-09-03/eco.owl` 

**IRI for codes**:
:code:`http://purl.obolibrary.org/obo/ECO_` 


EDAM namespace
**************

**Ontology IRI**:  
:code:`https://biomedit.ch/rdf/sphn-resource/edam/`

**Version IRI example**:
:code:`https://biomedit.ch/rdf/sphn-resource/edam/1.25` 

**IRI for codes**:
:code:`http://edamontology.org/` 


EFO namespace
**************

**Ontology IRI**:  
:code:`http://www.ebi.ac.uk/efo/efo.owl`

**Version IRI example**:
:code:`http://www.ebi.ac.uk/efo/releases/v3.72.0/efo.owl` 

**IRI for codes**:
:code:`http://www.ebi.ac.uk/efo/EFO_` 



EMDN namespace
**************

**Ontology IRI**:  
:code:`https://biomedit.ch/rdf/sphn-resource/emdn/`

**Version IRI example**:
:code:`https://biomedit.ch/rdf/sphn-resource/emdn/2021-09-29/1` 

**IRI for codes**:
:code:`https://biomedit.ch/rdf/sphn-resource/emdn/` 


GENO namespace
**************

**Ontology IRI**:  
:code:`http://purl.obolibrary.org/obo/geno.owl`

**Version IRI example**:
:code:`http://purl.obolibrary.org/obo/geno/releases/2022-08-10`

**IRI for codes**:
:code:`http://purl.obolibrary.org/obo/GENO_`


GENEPIO namespace
******************

**Ontology IRI**:  
:code:`http://purl.obolibrary.org/obo/genepio.owl`

**Version IRI example**:
:code:`http://purl.obolibrary.org/obo/genepio/releases/2023-08-19/genepio.owl`

**IRI for codes**:
:code:`http://purl.obolibrary.org/obo/GENEPIO_`


HGNC namespace
**************

**Ontology IRI**:  
:code:`https://biomedit.ch/rdf/sphn-resource/hgnc/`

**Version IRI example**:
:code:`https://biomedit.ch/rdf/sphn-resource/hgnc/20221107`

**IRI for codes**:
:code:`https://biomedit.ch/rdf/sphn-resource/hgnc/`


ICD-10-GM namespace
*******************

**Ontology IRI**:
:code:`https://biomedit.ch/rdf/sphn-resource/icd-10-gm/`

**Version IRI example**:
:code:`https://biomedit.ch/rdf/sphn-resource/icd-10-gm/2023/3` 

**IRI for codes**:
:code:`https://biomedit.ch/rdf/sphn-resource/icd-10-gm/`


ICD-O-3 namespace
*******************

**Ontology IRI**:
:code:`https://data.jrc.ec.europa.eu/collection/ICDO3_ontology_en`

**Version IRI example**:
:code:`https://data.jrc.ec.europa.eu/collection/ICDO3_ontology_en/2.0` 

**IRI for codes**:
:code:`https://data.jrc.ec.europa.eu/collection/ICDO3_O#`


LOINC namespace
***************

**Ontology IRI**:
:code:`https://biomedit.ch/rdf/sphn-resource/loinc/`

**Version IRI example** :
:code:`https://biomedit.ch/rdf/sphn-resource/loinc/2.74/1` 


**IRI for codes**:
:code:`https://loinc.org/rdf/`


OBI namespace
**************

**Ontology IRI**:
:code:`http://purl.obolibrary.org/obo/obi.owl`

**Version IRI example** :
:code:`http://purl.obolibrary.org/obo/obi/2024-10-25/obi.owl`

**IRI for codes**:
:code:`http://purl.obolibrary.org/obo/OBI_`


OncoTree namespace
******************

**Ontology IRI**:
:code:`https://biomedit.ch/rdf/sphn-resource/sphn/oncotree#`

**Version IRI example** :
:code:`https://biomedit.ch/rdf/sphn-resource/sphn/oncotree/2021-11-02`

**IRI for codes**:
:code:`https://biomedit.ch/rdf/sphn-resource/sphn/oncotree#`


ORDO namespace
**************

**Ontology IRI**:
:code:`https://www.orphadata.com/data/ontologies/ordo/last_version/ORDO_en_4.4.owl`

**Version IRI example** :
:code:`https://www.orphadata.com/data/ontologies/ordo/last_version/ORDO_en_4.4.owl`

**IRI for codes**:
:code:`http://www.orpha.net/ORDO/Orphanet_`


SNOMED CT namespace
*******************

**Ontology IRI**:
:code:`http://snomed.info/sct/900000000000207008#`

**Version IRI example**: 
:code:`http://snomed.info/sct/900000000000207008/version/20221231`


**IRI for codes**:
:code:`http://snomed.info/id/`


SO namespace
************

**Ontology IRI**:  
:code:`http://purl.obolibrary.org/obo/so.owl`

**Version IRI example**:
:code:`http://purl.obolibrary.org/obo/so/2021-11-22`

**IRI for codes**:
:code:`http://purl.obolibrary.org/obo/SO_`


UCUM namespace
**************

**Ontology IRI**:  
:code:`https://biomedit.ch/rdf/sphn-resource/ucum/`

**Version IRI example**:
:code:`https://biomedit.ch/rdf/sphn-resource/ucum/2023/1` 

**IRI for codes**:
:code:`https://biomedit.ch/rdf/sphn-resource/ucum/`







