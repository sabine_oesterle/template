SPHN Semantic Interoperability Framework
==================================================

.. toctree::
  :maxdepth: 2
  :caption: SPHN framework

  sphn_framework/introduction
  sphn_framework/sphndataset
  sphn_framework/sphnrdfschema
  sphn_framework/sphn_schemascope
  sphn_framework/schemaforge
  sphn_framework/sphn_connector
  sphn_framework/terminology_service
  sphn_framework/sphn_mocker

.. toctree::
  :maxdepth: 1
  :caption: External terminologies used
  
  external_terminologies/external_terminologies
  external_terminologies/versions_used_in_sphn
  external_terminologies/naming_conventions

  
.. toctree::
  :maxdepth: 2
  :caption: User guide - How to?
  
  user_guide/project_schema_generation
  user_guide/data_generation
  user_guide/data_quality
  user_guide/managing_graphdb
  user_guide/data_loading
  user_guide/graph_subset
  user_guide/data_exploration
  user_guide/sparql
  user_guide/data_analysis
  user_guide/terminology_service
  user_guide/project_terminologies

.. toctree::
  :maxdepth: 2
  :caption: Concepts guidelines

  concepts_guidelines/assessment_guidelines
  concepts_guidelines/lab_guidelines
  concepts_guidelines/measurement_guidelines
  concepts_guidelines/omics_guidelines
  concepts_guidelines/provenance_guidelines
  concepts_guidelines/drug_guidelines

.. toctree::
  :maxdepth: 2
  :caption: Examples of Implementation

  examples_of_implementation/chuv_implementation
  examples_of_implementation/hug_implementation
  examples_of_implementation/insel_implementation
  examples_of_implementation/usb_implementation
  examples_of_implementation/usz_implementation

.. toctree::
  :maxdepth: 2
  :caption: Background information

  background_information/clinical_interoperability_background
  background_information/semantic_web
  background_information/rdf_background
  background_information/nquads
  background_information/shacl_background
  background_information/sparql_background
  
.. toctree::
  :maxdepth: 2
  :caption: Other
  
  other/contactinformation
  other/acknowledgements
