Acknowledgements
=================

We would like to thank the SPHN and BioMedIT Working groups: Semantics, RDF and Research Support as well as 
all representatives of the Swiss University Hospitals (HUG, CHUV, USB, USZ and Insel) and 
SIB Swiss Institute of Bioinformatics for their contributions. 
