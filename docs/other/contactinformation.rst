Contact Information
=======================

For additional information or question, please contact the `SPHN DCC FAIR Data Team <https://www.biomedit.ch/home/about-us/phi-group.html>`_ at `fair-data-team@sib.swiss <mailto:fair-data-team@sib.swiss>`_.