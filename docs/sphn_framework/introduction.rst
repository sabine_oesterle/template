.. _framework-introduction:

Introduction 
============


.. note:: 
 > Watch the SPHN webinars introducing `the Framework and its Toolstack <https://www.youtube.com/watch?v=T8DHHJVmU4k>`_  
 and `SPHN Data Ecosystem for FAIR Data <https://www.youtube.com/watch?v=pqV0qp4oisM>`_!

.. warning::
  If you are not familiar with Semantic Web technologies and its related content (e.g., standards, formats, languages), 
  we strongly encourage you to read the Background Information section in this document.


To advance personalized health research, scientists need access to different types of health and related data. 
The challenge however is that data is commonly available at: 
  1) various sources like hospitals, laboratories, or even mobile devices (e.g. data from smart watch), and 
  2) in different formats.

This makes it hard for researchers to effectively use data (i.e.
difficult to collect, connect and understand data from different sources). 
To solve this issue, the `Swiss Personalised Health Network (SPHN) <https://sphn.ch/>`_ initiative originated. 
SPHN is a Swiss governement initiative that helps to bring together **data provider** (e.g. hospitals, health-care providers) 
and **researchers**. The aim is that data can be shared and reused in a coordinated and standard manner to support health studies.

SPHN Semantic Interoperability Strategy
---------------------------------------
SPHN provides a framework for **exchanging** health-related data in an **interoperable** way for research. 

.. note::
   Interoperable data means that data coming from different sources follow the same norm 
   and are compatible, comparable, linkable and usable together.

To facilitate interoperability, so-called standards and coding systems can be used to represent data.
Currently, only a few nationally adopted and implemented standards for medical information in hospitals. 
Some coding systems are used for patient billing and accounting.

To enable and facilitate the use of health-related data from clinical routine and other sources 
for research, SPHN has developed a  
`semantic interoperability strategy <https://sphn.ch/network/data-coordination-center/the-sphn-semantic-interoperability-framework/>`_ 
`[Gaudet-Blavignac et al. 2021] <https://medinform.jmir.org/2021/6/e27591/>`_ 
based on a three pillars strategy: 

* Pillar 1: SPHN Concepts (semantics)
* Pillar 2: SPHN RDF Schema (data transport and storage)
* Pillar 3: Use cases

.. image:: ../images/sphn_semantic_strategy.png
   :align: center
   :alt: DCC semantic interoperability

**Figure 1. Semantic interoperability strategy of SPHN.** 


The Stakeholder Roles 
---------------------

In the SPHN initiative and this documentation, the different roles are defined: 

Data Provider (alias the 'data producer') refers to someone like a **Clinical Data Manager**, who looks after data in hospitals or other clinical settings
and makes it available for researchers to use. 

Members of a scientific project (alias the 'data consumers') include: 

- **Project Data Manager**: a technical expert who prepares or extends data for researchers and specific scientific projects. 

- **Researcher**: a biomedical researcher who needs to access and study the data 

- **Project Leader**: a person in charge of a specific research project. 


These groups work together as part of the SPHN Ecosystem.

The `Data Coordination Center (DCC) <https://sphn.ch/network/data-coordination-center/>`_, 
managed by the `Personalized Health Informatics <https://sphn.ch/organization/implementation-teams/>`_ group, 
coordinates the building of infrastructures to support all these stakeholders.

SPHN Data Ecosystem for FAIR Data
---------------------------------
The SPHN initiative has developed an ecosystem to make it easier for 
researchers and healthcare providers to share and reuse health data `[Österle et al. 2021] <https://www.preprints.org/manuscript/202109.0505/v1>`_ . 
It contains several components that help to standardize the way data is represented and exchanged (see Figure 2):

  The DCC defines the `SPHN Dataset <https://sphn.ch/document/sphn-dataset/>`_,
  which describes the meaning of the data (i.e. semantics) to be represented. 
  For example, it describes and explains what information should be included to describe something like an allergy. 
  This should ensure that everyone is talking about the same thing.
  The SPHN Dataset semantically defines health-related concepts (or terms) 
  used in health research in Switzerland (Pillar 1). 
  Note that "dataset" here basically refers to *metadata* of the actual health data to be used, 
  e.g. attributes or field names defined in a clinical health record.

  To make it easier to share this information, the semantics defined in the SPHN Dataset are represented in a standard format called 
  `RDF (Resource Description Framework) <https://www.w3.org/RDF/>`_ (Pillar 2). 
  This allows data providers to shared their clinical data in a consistent and organized manner following standards, 
  like the `FAIR principles <https://www.go-fair.org/fair-principles/>`_ that promote making data easy to find, access, reuse and compatible across different sources.
  The result of this is the `SPHN RDF Schema <https://biomedit.ch/rdf/sphn-schema/sphn>`_.

  To connect the SPHN's schema with external (national and international) 
  standard terminologies and classifications, a :ref:`framework-terminology-service` 
  `[Krauss et al. 2021] <https://doi.org/10.3390/app112311311>`_ has been developed 
  to automatically convert existing health-related standards into the RDF format and 
  make them accessible to both data providers and data users (researchers) within the SPHN framework. 
  Their reuse in SPHN ensures that data is not only consistent within Switzerland but also increases compatibility with international standards when possible. 
  This strengthens and enriches the data, making it more reliable and easier to share or compare across sources.

  **Semantic and schema extension**: Scientific projects have the possibility to 
  :ref:`extend the semantics <userguide-project-schema>` defined in the SPHN Dataset 
  and RDF Schema by adding project-related information. 
  This extension process can happen in two ways: 

     either the project represents the semantics in the :ref:`SPHN Dataset Template <option1-dataset-template>` 
     and uses the :ref:`SPHN Schema Forge <framework-schemaforge>` 
     webservice to automatically generate the project-specific RDF Schema

     or the project builds directly their schema in the RDF format using an editor of their choice. 
     
  A Project Data Manager can then send the project-specific 
  RDF Schema to the data providers who integrate this new schema into their pipelines 
  for transforming clinical data warehouse data and 
  :ref:`generate RDF data files <userguide-data-generation>` that comply with the given RDF Schema. 
  Since 2023, data providers have the possibility to use the :ref:`SPHN Connector <framework-sphn-connector>` to build a valid SPHN-compliant RDF Schema.

  **Quality assurance**: Any new data generated by a clinical data manager 
  can be checked for quality. This is automatically done when using the SPHN Connector, which integrates the 
  `Quality Check tool <https://sphn-semantic-framework.readthedocs.io/en/latest/sphn_framework/dataquality.html>`_ 
  that integrates the :ref:`framework-shacler` and SPARQL queries. 

  **Data reuse**: Once data is validated, researchers can :ref:`explore and analyze <user-guide-data-exploration>` 
  the data as they need.


.. image:: ../images/sphn-overview.*
   :align: center
   :alt: SPHN Ecosystem
   
**Figure 2. Simplified overview of the SPHN Ecosystem.** 
In blue colored text are elements developed and taken care by the DCC. 
In orange colored text are tasks that should be achieved by the scientific project members. 
In red colored text are tasks that should achieved by the data providers.


Reference
---------------------------------
Österle, S.; Touré, V.; Crameri, K. (2021), The SPHN Ecosystem Towards FAIR Data. 
Preprints, 2021090505 (`doi: 10.20944/preprints202109.0505.v1 <https://www.preprints.org/manuscript/202109.0505/v1>`_)

Gaudet-Blavignac C, Raisaro JL, Touré V, Österle S, Crameri K, Lovis C (2021), 
A National, Semantic-Driven, Three-Pillar Strategy to Enable Health Data Secondary Usage 
Interoperability for Research Within the Swiss Personalized Health Network, 
JMIR Med Inform 9 (6), e27591 (`doi: 10.2196/27591  <https://medinform.jmir.org/2021/6/e27591>`_)

Touré, V., Krauss, P., Gnodtke, K., Buchhorn, J., Unni, D., Horki, P., Raisaro, J.L., Kalt, K., 
Teixeira, D., Crameri, K. and Österle, S. (2023), 
FAIRification of health-related data using semantic web technologies in the Swiss Personalized Health Network. 
Scientific Data, 10(1), p.127 (`doi: 10.1038/s41597-023-02028-y <https://doi.org/10.1038/s41597-023-02028-y>`_)
