.. _sphn_mocker:

SPHN Mocker
================

The SPHN Mocker is a Python-based tool that generates mock data according to the SPHN RDF Schema.
The output generates JSON and Turtle mock data files, 
where each file contains mock information about a supposed patient (i.e. Subject Pseudo Identifier).

The SPHN Mocker can be run through a CLI (Command Line Interface) or with Docker.
It is flexible in the mock data being generated as the user can: 

  - specify narrow the restrictions to apply for specific properties
  - restrict the data to generate a subset of classes
  - generate as many "patients" as needed (keep in mind that 1 file contains mock  data for 1 patient).



The SPHN Mocker depends on the following tools:

 - :ref:`sphn-sampler`
 - :ref:`framework-sphn-connector`

Since version 1.1.6, the SPHN Mocker supports the generation of mock data from the SPHN RDF Schema 2024.2.
Note that project-specific schemas are not supported yet.

.. _sphn-sampler:

SPHN Sampler 
----------------

The SPHN Sampler is a Python-based tool that provides for a given SPHN property, the set of possible values.
It takes as input a RDF Schema and external terminologies and gives as output a JSON file that contains the samples needed for the :ref:`sphn_mocker`.


Availability and usage rights
-----------------------------

© Copyright 2025, Personalized Health Informatics Group (PHI), SIB Swiss Institute of Bioinformatics

The SPHN Mocker is available at 
https://git.dcc.sib.swiss/sphn-semantic-framework/sphn-mocker and 
is licensed under the `GPLv3 <https://www.gnu.org/licenses/gpl-3.0.en.html>`_ license.

The SPHN Sampler is available at 
https://git.dcc.sib.swiss/sphn-semantic-framework/sphn-sampler and 
is licensed under the `GPLv3 <https://www.gnu.org/licenses/gpl-3.0.en.html>`_ license.

For any question or comment, please contact the SPHN DCC FAIR Data Team at `fair-data-team@sib.swiss <mailto:fair-data-team@sib.swiss>`_.