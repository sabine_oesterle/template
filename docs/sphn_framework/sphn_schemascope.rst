.. _framework-schemascope:

SPHN Schema Scope
=================

The SPHN Schema Scope, the SPHN metadata catalogue, is available for discovering and exploring the SPHN core dataset and
datasets from SPHN-funded projects, allowing to explore individual concepts in the full context of the schema graph. 
It offers researchers a highly interactive way to visualize data schemas, simplifying the navigation 
of even the most complex data structures.

The SPHN Schema Scope web service is available at: https://schemascope.dcc.sib.swiss/.

The figure below displays the start page of the SPHN Schema Scope:

.. figure:: ../images/schemascope/schema_scope_overview_01.png
 :height: 350 
 :align: center 
 :alt: SPHN Schema Scope graphical user interface overview

**Figure 1. The SPHN Schema Scope graphical user interface.**  



Basic overview
--------------
The graphical user interface of SPHN Schema Scope features two parts:

* **Control elements** for the graph display (→ left side) [jump to :ref:`section <framework-control-elements>`]

     * schema selection
     * selection of a concept subset of interest
     * advanced options of graph display (graph setup, inheritance)
     * general display options (coloring, display dimensions, labels, type of shapes)


* **Schema graph, additional metadata, and help** (→ tabs on right side) [jump to :ref:`section <framework-graph-and-metadata>`]

    * schema graph
    * table view of the underlying schema
    * general schema information, including

        * version of the schema
        * licenses of terminologies used

    * Help-tab for a quickstart introduction
    * (FAIR Data Point *Please note that depending on the access modalities and the selected project this tab may not
      be available.)*

 .. note::
 
    The **control elements** allow to tailor the graph view to the user's needs. 
    Most changes will have an **immediate impact** on the way the graph is displayed.

    **Specific regions of the graph** can be explored in the graph tab. The **mouse wheel** allows **zooming in or out**, 
    and the **graph can be dragged** to an area of interest by holding the left mouse button and moving the mouse.

|

.. _framework-control-elements:

Control elements
----------------

Control elements are primarily located in the control sidebar on the left of the display.


Schema selection
*****************

The user can select a schema to be visualized from the dropdown menu (**"Choose a schema:"**). The choice features SPHN schemas from release 2023.2 onwards and project-specific schema from National Data Stream (NDS) and Demonstrator projects.

.. image:: ../images/schemascope/schema_scope_schema_selection_02_annotated.png
 :width: 80% 
 :align: center 
 :alt: Schema selection in SPHN Schema Scope

**Figure 2. Selecting a schema of interest in SPHN Schema Scope.**  


Showing subsets of a graph
**************************

SPHN Schema Scope offers the possibility to explore subgraphs to simplify the navigation of complex data structures.

.. image:: ../images/schemascope/schema_scope_concept_selection_01a_annotated.png
 :height: 250
 :align: center 
 :alt: Concept selection in SPHN Schema Scope

**Figure 3. Selecting concepts of interest in SPHN Schema Scope.**


.. _framework-concept-selection:

**Concept selection**

Uncheck the box "**Show complete graph**" to enable the selection of a subset of concepts of interest including 
additional options.

.. note::

   Select the concepts of interest (Fig. 3A) by choosing from the dropdown menu ("Select concepts") or by typing in the selection box.
   The search is case insensitive and will pick up partial matches of the search term regardless of its position, e.g., 
   suggestions for a search of "he" will include **He**\ art Rate, **He**\ art Rate Measurement, but also Body **He**\ ight 
   or Radiot\ **he**\ rapy Procedure.


**Depth of connections**

Selection of the number of "hops" in the field **"Depth of connections to display from selected concepts"** (Fig. 3B) in the
graph to be shown, starting from the selected concepts.


.. image:: ../images/schemascope/depth_of_connections_Age_depth_1-3_annotated_plus_text.png
 :width: 100%
 :align: center
 :alt: Schema selection in SPHN Schema Scope

**Figure 4. Depths of connections 1 through 3 shown for concept "Age".**

A depth of 1 (left panel) shows directly connected nodes only, while a depth of 2 (center panel) 
and 3 (right panel) shows nodes which are 1 or 2 additional hops away from the selected concept(s), respectively.


**Directionality of edges**

Selection of the directionality of links between nodes to be shown ("Directionality of edges", Fig. 3C):

      * attributes of the selection, i.e., forward edges ("forward only")
      * concepts using the selection, i.e., reverse or backward edges ("backward only")
      * or both forward and reverse ("both directions")

.. image:: ../images/schemascope/directionality_Sample_annotated.png
  :width: 100%
  :align: center
  :alt: Schema selection in SPHN Schema Scope

**Figure 5. Effect of directionality-option**

| Figure 5 illustrates the effect of the different directionality options, exemplified for the ``Sample`` concept (release 2024.2).
  Note that the advanced option "Avoid edges hiding each other?" (see Fig. 6C) was enabled to create the views of Fig. 5.
| The left panel shows all "forward" edges starting from ``Sample``, i.e., the attributes of the concept like the 
  identifier, material type code, or the body site. The central panel shows the edges targeting or directed "backward" to 
  ``Sample``, i.e., it shows edges of concepts using ``Sample``, including ``Sample Processing``, ``Library Preparation``, 
  ``Assay``, or ``Lab Test Event``. The right panel combines the two options showing both directions, therefore all connections from and to the ``Sample``.


.. note::

   **Increasing the depth or selecting a directionality of "both directions" may quickly expand the subgraph** depending on 
   the initial selection. Once the area of interest is outlined it may be helpful to expand the initial concept selection 
   by a few concepts along the specific interest to reduce the need to overly increase the depth and to avoid expanding the 
   subgraph unintentionally.

|


Advanced options
****************

Check the box "**Show advanced options**" to display additional advanced options for the graph setup.

.. image:: ../images/schemascope/schema_scope_advanced_options_01_annotated.png
 :height: 300 
 :align: center 
 :alt: Advanced graph options in SPHN Schema Scope

**Figure 6. Selecting advanced graph options in SPHN Schema Scope.**  


\"Display inheritance links? (Fig. 6A)\"
  - Option to show the links between concepts and their parents
  - Examples: Links from  ``Implant`` to ``Medical Device`` or ``Reference Range`` to ``Range``

.. _framework-node-spacing-settings:

\"Simulated node repulsion? (Fig. 6B)\"
  - Option to choose between dynamic node spacing (distance controlled by "edge length" and "node repulsion")
    or static node positions
  - Static node positions require a manual spacing of the nodes which may be cumbersome if many nodes are displayed.
  - Individual nodes can be dragged with both settings. However, if node repulsion if enabled, a node may be repelled 
    when moved too closed to another one. This results in a rearrangement of the graph.

\"Avoid edges hiding each other? (Fig. 6C)\"
    - Edges tend to overlap when a lot of nodes are shown or multiple attributes of a concept are of the same type
    - Overlapping of edges can be strongly reduced with this option.
    - Nodes tend to be spaced far wider when this feature is enabled, adjusting :ref:`node repulsion <framework-node-repulsion>`
      may help to compact the graph display

\"Edge length (Fig. 6D)\"
    - Determines lengths of the connecting edges, higher values (arbitrary scale) increase the edge length

.. _framework-node-repulsion:

\"Node repulsion (Fig. 6E)\"
    - Determines spacing of nodes, higher values (arbitrary scale) increase the spacing

|


Display options
****************

Check the box "**Show display options**" to show additional options for the general graph display.

.. image:: ../images/schemascope/schema_scope_display_options_01_annotated.png
 :height: 300 
 :align: center 
 :alt: Display options in SPHN Schema Scope

**Figure 7. Selecting display options in SPHN Schema Scope.**  


\"Color core concept groups? (Fig. 7A)\"
    - If enabled, nodes of several core concept families will be displayed with distinct colors
    - The following coloring applies: ``Result`` (light blue), ``Lab Test`` (light orange), ``Assessment`` (light pink), ``Medical Procedure`` (green),
      ``Diagnosis`` (dark orange), ``Measurement`` (yellow)
    - **Examples**: ``Assessment`` and ``Assessment Event`` are shown in light pink, ``Body Weight Measurement`` and 
      ``Heart Rate Measurement`` are shown in yellow, and ``Body Weight``, ``Heart Rate`` and ``Assessment Result`` 
      are shown in light blue.

\"Show cardinalities in property label? (Fig. 7B)\"
    - The cardinality of the connection of two nodes (minimum and maximum number of links) is usually displayed when hovering 
      over en edge. If this option is selected it will be shown below the edge label instead.

\"Display width (pixels) (Fig. 7C)\" and \"Display height (pixels) (Fig. 7D)\"
    - Changing the display dimensions may be beneficial on large screens or when the control sidebar is hidden 
      (see Fig. 8 below)

\"Node shape (Fig. 7E)\"
    - Shape of the nodes can be selected, either boxes or circles
    - The label is shown inside the node for boxes and below for circles.


.. image:: ../images/schemascope/control_sidebar_toggle_01_annotated.png
  :width: 75%
  :align: center
  :alt: Display sidebar toggle in SPHN Schema Scope

**Figure 8. Toggling the display of the control sidebar**

|


.. _framework-graph-and-metadata:

**Graph and metadata display**
------------------------------

The right part of the graphical user interface features the graph display and several additional metadata resources. 
This part of the display can be expanded by hiding the control sidebar (see Fig. 8 above).

.. image:: ../images/schemascope/schema_scope_graph_and_metadata_display_annotated_02.png
  :width: 75%
  :align: center
  :alt: Display of graph and metadata in SPHN Schema Scope

**Figure 9. Overview of graph and metadata display options in SPHN Schema Scope**


Graph
*********

| In the "Graph" tab (Fig. 9A) a schema graph can be explored. The **mouse wheel** allows 
  **zooming in or out**, and the **graph can be dragged** to an area of interest by holding the left mouse button and 
  moving the mouse.
| Individual nodes can also be dragged. Depending on the settings for :ref:`node repulsion <framework-node-spacing-settings>`, 
  they may be static or repelled when moved too closed to another node. The latter results in a rearrangement of the graph.
| Depending on potential :ref:`concept selections <framework-concept-selection>`, only a subgraph of interest may be shown.
| To quickly locate a particular node in the graph, select it in the **"Select by label" dropdown menu** (upper left in 
  the Graph tab) by scrolling to the the relevant concept or by typing the starting letters. The selected node is subsequently 
  highlighted. It may, however, be necessary to zoom out and in to eventually home in on the region of interest.



Table view
**********

| The "Table view" tab (Fig. 9B) features a tabular display of the schema underlying the graph, including node types, 
  standards used, count data (*if applicable*), and descriptions of classes and predicates.
| Note that for project-specific schemas the concepts of the SPHN core schema are only shown to the extent they are 
  reused.
| A search box (upper right) allows to identify information regarding a specific class or property of interest.


Schema information
******************

Click the tab "Schema information" tab (Fig. 9C) to reveal its subtabs "Schema version" and "Terminology licenses":

Schema version (Fig. 9E)
    - Version and license information for the SPHN Schema underlying the graph
    - Version and license information for the project-specific schema *(if applicable)*
    - Note that the two version numbers are independent!

Terminology licenses (Fig. 9F)
    - Information on the licenses of the external terminologies used by the selected schema.
    - A search box (upper right) allows to identify information for a specific license of interest.


Help
****

The "Help"-tab (Fig. 9D) features a quickstart introduction including screenshots of the key elements and a link to this 
extended documentation.


FAIR Data Point
***************

| A FAIR Data Point (FDP) is a REST API (**A**\ pplication **P**\ rogramming **I**\ nterface built according to the 
  design principles of the REST-architecture (**Re**\ presentational **S**\ tate **T**\ ransfer)) for creating, storing, 
  and serving FAIR metadata.
| Depending on the access modalities and the selected project the "FAIR Data Point"-tab may become accessible next to 
  the "Help"-tab and allows to proceed to the `SPHN FAIR Data Point <https://fdp-schemascope.dcc.sib.swiss/>`_.

|

Availability and usage rights
-----------------------------

© Copyright 2025, Personalized Health Informatics Group (PHI), SIB Swiss Institute of Bioinformatics.

The ``SPHN Schema Scope`` is available at: https://schemascope.dcc.sib.swiss/ and is licensed under the `GPLv3 <https://www.gnu.org/licenses/gpl-3.0.en.html>`_  license.

For any questions or comments, please contact the SPHN DCC FAIR Data Team at 
`fair-data-team@sib.swiss <mailto:fair-data-team@sib.swiss>`_.
