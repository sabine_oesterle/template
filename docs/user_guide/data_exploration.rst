.. _user-guide-data-exploration:

Visually explore data with GraphDB
==================================

.. note:: 

 To find out more watch the `Schema and Data Visualization Training <https://sphn.ch/training/datavisualization/>`_  


Target Audience
---------------

This document is mainly intended for researchers who are
interested in exploring with visuals their data using 
the GraphDB triplestore. This document provides
information about data loading and visualization of both 
the schema and the data in GraphDB.


Schema and data visualization
-----------------------------


.. _mock-data description:

Mock-data
!!!!!!!!!

In order to demonstrate the visualization capabilites of GraphDB some mock-data will be used, 
an overview of which is shown in `Figure 13`_. The mock-data is modeled with the SPHN RDF Schema 
and centered around patients, denoted by the class ``SubjectPseudoIdentifier``. 
In this mock-data, each patient has an ``AllergyEpisode``, triggered by an ``Allergen``, 
and confirmed by a ``LabTestEvent``. 
Codes from the external terminologies :ref:`external-terminologies-snomed-ct` 
are used for encoding substances, :ref:`external-terminologies-loinc` 
for encoding laboratory tests, and :ref:`external-terminologies-ucum` 
for encoding units of measurement. 

.. _Figure 13:

.. figure:: ../images/graphdb_explore/mock-data_overview.png
   :width: 800px
   :align: center
   :alt: Mock-data overview.
   
**Figure 13: Mock-data overview.**

Class hierarchy
!!!!!!!!!!!!!!!

Shown in `Figure 14`_  is a class hierarchy visualization in GraphDB, 
with a focus on the classes from the SPHN RDF Schema used in the mock-data. 
Here, the levels in the hierarchy are represented by packing circles inside other circles (nested structure).
Further information on class hierarchy visualization can be found in GraphDB's 
`documentation <https://graphdb.ontotext.com/documentation/10.2/getting-started.html?highlight=class%20hierarchy#class-hierarchy>`_.

.. _Figure 14:

.. figure:: ../images/graphdb_explore/Screenshot-class-hierarchy.png   
   :align: center
   :alt: Class hierarchy visualization.
   
**Figure 14: Class hierarchy visualization.**

Class relationships
!!!!!!!!!!!!!!!!!!!

Shown in `Figure 15`_  is a visualization of class hierarchy relationships in GraphDB. 
Here, the relationship between instances of classes are depicted as bundles of links in both directions. 
The bundles vary in thickness (indicating the number of links), and in color 
(indicating the class with the higher number of incoming links). 
Only the classes with the most ingoing and/or outgoing links are included per default. 
Classes can be added/removed by clicking on the corresponding icons.

For the mock-data used in this example, we find that the ``Quantity`` class is 
tied for the top spot regarding the total number of links. 
It is strongly connected to the ``Unit`` class, 
and has both incoming and outgoing links.

The ``AllergyEpisode`` class, on the other hand, only has outgoing links and 
connects to the ``DataProvider``, ``BodySite``, etc.

Further information on class relationships visualization can be found in 
GraphDB's `documentation <https://graphdb.ontotext.com/documentation/10.2/getting-started.html?highlight=class%20hierarchy#class-relationships>`_.

.. _Figure 15:

.. figure:: ../images/graphdb_explore/Screenshot-class-relationships.png   
   :align: center
   :alt: Class relationships visualization.
   
**Figure 15: Class relationships visualization.**

Visual graph
!!!!!!!!!!!!

The GraphDB visual graph functionality enables the visualization of a specific class 
or data of interest that was imported. For example, in `Figure 16`_ the search for 
``LabResult`` is shown, along with suggestions provided by the Autocomplete functionality (`more information`_). 

.. _Figure 16:

.. figure:: ../images/graphdb_explore/Screenshot-LabResult-autocomplete.png   
   :align: center
   :alt: Search for visual graph of the LabResult class.
   
**Figure 16: Search for visual graph of the LabResult class.**


Following the search for the ``LabResult``, 
the corresponding class is shown along with its first hop neighbours. 
Both the imported RDF Schema and instances of ``LabResult`` (purple nodes) 
are included in the displayed visual graph (see `Figure 17`_). 

.. _Figure 17:

.. figure:: ../images/graphdb_explore/Screenshot-LabResult-Visual-Graph-all.png
   :align: center
   :alt: Visual graph for the LabResult class.
   
**Figure 17: Visual graph for the LabResult class.**


.. note:: 
   
 Only the first 20 links to other resources are shown per default. 
 This limit, as well as the types and predicates being shown, 
 can be be adjusted in the settings (see `Figure 18`_).

.. _Figure 18:

.. figure:: ../images/graphdb_explore/Screenshot-Visual-Graph-settings-composed.png
   :width: 438
   :align: center
   :alt: Settings for the visual graph display.
   
**Figure 18: Settings for the visual graph display.**


Through the settings one can, for example, exclude all instances of 
the ``sphn:LabResult`` class (i.e., by adding it to the *Ignored types* - type ``sphn:LabResult``, press enter) 
yielding a visual graph of the ``LabResult`` schema only (see `Figure 19`_ ). 
Here, in addition to classes, object and datatype properties (blue) are shown. 
The object properties link instances of classes to other instances 
(e.g., ``LabResult`` to ``Quantity`` by ``hasQuantitativeResult``). 
The datatype properties link instances of classes to literal values 
(e.g., ``LabResult`` to ``dateTime`` by ``hasReportDateTime``).

.. _Figure 19:

.. figure:: ../images/graphdb_explore/Screenshot-LabResult-Visual-Graph-schema.png
   :align: center
   :alt: LabResult schema.
   
**Figure 19: LabResult schema.**

One can also search for instances of a class, as shown in `Figure 20`_.

.. _Figure 20:

.. figure:: ../images/graphdb_explore/Screenshot-labresult-instance-autocomplete.png
   :align: center
   :alt: Search for visual graph of an instance of the LabResult class.
   
**Figure 20: Search for visual graph of an instance of LabResult class.**

A closer inspection of the visual graph for an instance of the 
``LabResult`` class (e.g., ``CHE-229_707_417-LabResult-e8624fab-5186-4e32-ae8d-7ee9770348a0`` in `Figure 21`_) 
reveals object property links to many instances such as a ``LabTest``, ``ReferenceRange``, ``Sample`` etc.

.. _Figure 21:

.. figure:: ../images/graphdb_explore/Screenshot-labresult-Visual-Graph-instance.png
   :align: center
   :alt: Visual graph for an instance of the LabResult class.
   
**Figure 21: Visual graph for an instance of the LabResult class.**

By clicking once on a LabResult instance (e.g., ``CHE-229_707_417-LabResult-e8624fab-5186-4e32-ae8d-7ee9770348a0``) 
a side panel appears, providing additional information as shown in `Figure 22`_. 

.. note::
   In addition to annotations (label, description, etc.) side panel 
   also contains datatype properties along with their values.

.. _Figure 22:

.. figure:: ../images/graphdb_explore/Screenshot-labresult-sidepanel.png
   :width: 454
   :align: center
   :alt: Side panel for an instance of the LabResult class.
   
**Figure 22: Side panel for an instance of the LabResult class.**

Double clicking on a node expands it by showing its first hop neighbours, 
as demonstrated in `Figure 23`_  for the ``Code-LOINC-804-5`` instance. 
Note that a single code instance is shared among different ``LabResult`` instances. 
One can learn more about the LOINC code either by inspecting the side panel, 
or by visiting the `URI of the LOINC code <https://loinc.org/rdf/804-5>`_.

.. _Figure 23:

.. figure:: ../images/graphdb_explore/Screenshot-labresult-Visual-Graph-loinc-code.png
   :align: center
   :alt: Exploring a LOINC code instance in a visual graph.
   
**Figure 23: Exploring a LOINC code instance in a visual graph.**

One can also explore the UCUM code instance which is connected to the ``Quantity``  class,
and learn that it has ``cal`` as unit code (see `Figure 24`_ ).

.. _Figure 24:

.. figure:: ../images/graphdb_explore/Screenshot-UCUM-unit.png
   :align: center
   :alt: Exploring an UCUM code instance in a visual graph.
   
**Figure 24: Exploring an UCUM code instance in a visual graph.**

Now in order to find out more about what is causing the allergy, 
we need to traverse the visual graph by visiting ``SubjectPseudoIdentifier``, 
``AllergyEpisode``, and ``Allergen`` instances (see `Figure 25`_). 
Here, we find that the ``Allergen`` instance is linked to a SNOMED CT code instance, 
and in `Figure 26`_ we observe that this code is of type ``Egg white``. 

.. _Figure 25:

.. figure:: ../images/graphdb_explore/Screenshot-SubjectPseudoIdentifier-Allergy-Visual-Graph.png
   :align: center
   :alt: Exploring a SNOMED CT code instance in a visual graph.
   
**Figure 25: Exploring a SNOMED CT code instance in a visual graph.**

.. _Figure 26:

.. figure:: ../images/graphdb_explore/Screenshot-Code-SNOMED-CT-egg-white.png
   :align: center
   :alt: SNOMED CT code of Egg White.
   
**Figure 26: SNOMED CT code of Egg White.**

Once again, we can learn more by visiting the `URI of the SNOMED CT code <http://snomed.info/id/256443002>`_.


Querying and aggregating data for visualization
!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!

Similarly to querying relational databases using SQL, 
one can also query RDF graph databases using SPARQL. 
The queried data can then be aggregated for visualization, e.g., 
with the built-in Google Chart functionality in GraphDB. 
An example of this process is shown in `Figure 27`_, 
where the mock-data is queried for instances of classes. 
The retrieved instances are then aggregated per class, 
and the aggregated counts are visualized using Google Chart.

.. _Figure 27:

.. figure:: ../images/graphdb_explore/Screenshot-Google_Chart.png
   :align: center
   :alt: Example of querying and aggregating data for visualization using SPARQL and Google Chart.
   
**Figure 27: Example of querying and aggregating data for visualization using SPARQL and Google Chart.**


Availability
------------

The SPHN RDF Schema is available in `git <https://git.dcc.sib.swiss/sphn-semantic-framework/sphn-schema>`_ 
(the visual documentation is accessible `here <https://www.biomedit.ch/rdf/sphn-schema/sphn>`_).

The mock data is available 
in `GitLab <https://git.dcc.sib.swiss/sphn-semantic-framework/sphn-training-material/-/blob/master/mock_data/mock-data-exploration.zip>`_.

External terminologies are available through the 
Terminology Service accessible on the 
`BioMedIT Portal <https://portal.dcc.sib.swiss/>`_ (for additional information, 
please read about the :ref:`framework-terminology-service`).

