.. _user-guide-terminology-service:

Download external terminologies from the Terminology Service
============================================================

.. note::

 For background information about the Terminology Service, see the :ref:`framework-terminology-service` section.


Target Audience
---------------
 
The DCC distributes :ref:`external-terminologies` via 
(i) the Terminology Service in the 
`BioMedIT portal <https://terminology.dcc.sib.swiss/>`_ and 
(ii) a standalone 
`Terminology Server <https://terminology-server.dcc.sib.swiss/>`_ based on 
`MinIO <https://min.io/>`_. 
The first option is intended to be used by researchers; the second one by 
BioMedIT nodes and data providers. 
The following document shows how to download these external terminologies
in each distribution. 


Downloading external terminologies from the BioMedIT portal
-----------------------------------------------------------

Prerequisites
*************
* You have a `SWITCH edu-ID <https://www.switch.ch/edu-id/>`_ account. 
  (If you don't have one, you can quickly generate one following instructions
  `here <https://login.eduid.ch/idp/profile/SAML2/Redirect/SSO?execution=e1s2>`_.)

Download
********
After logging in to the BioMedIT portal https://terminology.dcc.sib.swiss/ with 
your SWITCH edu-ID account, you will see a table with the available external 
terminologies (see Figure 1). Click on the terminology you want to download to expand the entry.
You will be presented with a list of available versions including buttons for 
the available file formats (``RDF/OWL`` and/or ``RDF/TTL``). Clicking on the 
button will download the terminology file.  


.. image:: ../images/terminology_service/portal-view.png
   :align: center
   :alt: Portal view of the terminology service

**Figure 1. Terminology Service in the BioMedIT Portal.** The content of the ICD-10-GM classification has been expanded, showing some of the available versions that can be downloaded.


Downloading external terminologies from the Terminology Server
--------------------------------------------------------------

Prerequisites
*************
* You have requested a user account for the Terminology Server at biomedit@sib.swiss.
* You have accepted the "Acceptable use policy for the Terminology Service".

Download via the web interface
******************************
After logging in at https://terminology-server.dcc.sib.swiss/ with your 
credentials, you will be presented with the MinIO Browser. It will show two 
folders (see Figure 2): ``current/``, which has the most recent release of the external 
terminologies, and ``archive/``, which has all previous releases.


.. image:: ../images/terminology_service/minio-view.png
   :width: 700px
   :align: center
   :alt: MinIO view of the terminology service

**Figure 2. Current and archive folders in the Terminology Server.** 

After navigating into one of the folders, you will be presented with a list 
of available packages. A package bundles all available external terminologies 
and consists of a ``.zip`` and a ``.gpg`` file (for instance,
``ontologies-2024-07-08_12-06-22.zip `` and 
``ontologies-2024-07-08_12-06-22.gpg``).
The ``.zip`` holds all the terminology files while the ``.gpg`` file is a 
detached gpg signature that allows to verify that the ``.zip`` file has been 
created by the DCC.

Select all relevant files and download them by clicking 
the ``Download`` button appearing on the right side of the screen.

After the download is complete, you can unzip the downloaded file and optionally 
verify the signatures of the packages.

.. note:: 
 **Verify the signature**

 Verifying a package's gpg signature is automatically done if using the 
 ``terminology-server-downloader`` (see below). To manually verify the 
 signature, download the "DCC" public gpg key from the 
 `Open PGP keyserver <hkps://keys.openpgp.org>`_ by running 

 .. code-block:: bash

   $ gpg --keyserver hkps://keys.openpgp.org --recv-keys CB5931C8B32B4BB7DE6D781C95B54DA2D018F7E7
 
 To verify a signature, run ``gpg --verify``, for instance:

 .. code-block:: bash
    
    $ gpg --verify ontologies-2024-07-08_12-06-22.gpg ontologies-2024-07-08_12-06-22.zip 
      gpg: Signature made Mo  8 Jul 12:08:33 2024 CEST
      gpg:                using EDDSA key CB5931C8B32B4BB7DE6D781C95B54DA2D018F7E7
      gpg: Good signature from "DCC <dcc@sib.swiss>" [unknown]
      gpg: WARNING: This key is not certified with a trusted signature!
      gpg:          There is no indication that the signature belongs to the owner.
      Primary key fingerprint: CB59 31C8 B32B 4BB7 DE6D  781C 95B5 4DA2 D018 F7E7

 The output below indicates that the file has been signed by the DCC.
 
 An output that includes

 .. code-block:: bash   

    gpg: BAD signature from "DCC <dcc@sib.swiss>" [unknown]

 indicates that the integrity of the data cannot be verified and that the data 
 should not be used.


Finally, unzip the ``ontologies-*.zip`` file to access the terminology files.


Download via the command-line interface
***************************************
The ``terminology-server-downloader``, accessible in 
`Git <https://git.dcc.sib.swiss/sphn-semantic-framework/terminology-server-downloader/>`_ 
can be used to download terminologies from 
the Terminology Server via a command-line interface. 


After installing this Python tool via ``pip`` and providing the credentials to 
the Terminology Server via environmental variables, all or a selected set of 
ontologies can be downloaded with a single command without further user 
interaction.

.. code-block:: bash   

   $ terminology-server-downloader ~/data

This process can further be automatized by invoking the command regularly from 
a ``cron`` job.

For further information, see the ``terminology-server-downloader`` 
`repository <https://git.dcc.sib.swiss/sphn-semantic-framework/terminology-server-downloader/>`_.

